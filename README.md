# Radiomics Enabler® - Unleash the potential of your medical images archives (PACS) for research

### General Information ###

Radiomics Enabler® is a web server connected to a PACS (Picture Archiving and Communication System) using a DICOM Query/Retrieve connection. A user can easily search and select sequences to extract. The extraction is then automated and sequences are placed in a directory. One can then use the [RSNA's Clinical Trials Processor](https://www.rsna.org/ctp.aspx) to process and anonymize the data. 

Radiomics Enabler® is a registered trademark. General information, video and installation package can be found at [www.medexprim.com/RadiomicsEnabler](www.medexprim.com/RadiomicsEnabler).

Information on dependencies, instuctions for building a Radiomics Enabler® server and configuring it can be found on the [Wiki](https://bitbucket.org/medexprim/radiomics-enabler/wiki/).


### Supported Platforms ###

Currently, the officially supported platforms are:

* Debian version 8 and 9
* Ubuntu 16

### Licensing ###

Radiomics Enabler® is licenced under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).If the AGPL is too restrictive for your usage, please contact us (contact@medexprim.com) to discuss the possibility of buying a license exception.

We also kindly ask scientific works and clinical studies that make use of Radiomics Enabler® to cite Radiomics Enabler® in their associated publications. Similarly, we ask open-source and closed-source products that make
use of Radiomics Enabler® to warn us about this use.

You may use the following citation :

Seymour K, Payoux P. **Radiomics Enabler®, an ETL (Extract-Transform-Load) for biomedical imaging in big-data projects**. In: IRIT, ed. *Symposium Sur l’Ingénierie de l’Information Médicale*. Toulouse, France; 2017. Available at: [https://www.irit.fr/SIIM/2017/SIIM2017_paper_5.pdf](https://www.irit.fr/SIIM/2017/SIIM2017_paper_5.pdf)


### Content ###

This archive contains the following directories:

* sitebase            - the Django project directory
* RadiomicsEnabler    - the Radiomics Enabler application
* extras              - Apache2, celery daemon, and storescp daemon files
* scripts             - some useful scripts for testing
* testInputs          - some test file inputs to be used for querys of the test server

This archive contains the following files:

* AUTHORS             - The list of the authors
* COPYING             - The AGPLv3 license
* README              - This file
* THANKS              - The list of the contributors
* NEWS                - The history of main changes between versions
* manage.py           - The runtime file for the Radiomics Enabler application

We have decided not to maintain a separate "ChangeLog" file. Each
commit to the official Radiomics Enabler® repository should be
associated with a description of the changes.


### Contributions and contact ###

We encourage external contributions, but just have not set things up yet. If you would like to contribute, please contact us at contact@medexprim.com. 
>>>>>>> django2port
