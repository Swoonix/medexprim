FROM python:3.7-alpine

# Runtime dependencies
RUN apk add --no-cache \
  libxslt \
  postgresql-libs

RUN apk add --no-cache --virtual .fetch-deps git && \
  git clone https://gitlab.com/Swoonix/medexprim.git /radiomics && \
  apk del .fetch-deps

WORKDIR /radiomics

RUN apk add --no-cache --virtual .build-deps \
  libffi-dev \
  zlib-dev \
  libxslt-dev \
  postgresql-dev \
  gcc \
  python3-dev \
  musl-dev \
  g++ && \
  pip install --no-cache-dir $(grep -vE "pkg-resources|mod-wsgi" requirements.txt) gunicorn && \
  apk del .build-deps

CMD [ "gunicorn", "-b", "0.0.0.0", "--pythonpath", "sitebase", "wsgi" ]