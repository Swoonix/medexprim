# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.1.31','192.168.1.23']
DEBUG = True
RAD_ENAB_EXTERNAL_VIEWER_USE = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'radiomics14',
        'USER': 'radenab14',
        'PASSWORD': 'radenab14',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

CORS_ORIGIN_WHITELIST = ( 'http://127.0.0.1:7014')



# RADIOMICS Enabler Configuration Settings

# the directory where all of the downloaded (CMOVE) data is stored
BASEDIR = '/mnt/bigdisk/data16/mike16/radenab/data/'
RAD_ENAB_DATA_DIR = BASEDIR +'RadEnab14/data'

# the temporary working directory where the temporary data is stored.
RAD_ENAB_WORK_DIR = BASEDIR + 'RadEnab14/work/'

# MEDIA ROOT for uploaded files
MEDIA_ROOT = BASEDIR + 'RadEnab14/mediaRoot'


STATIC_ROOT = BASEDIR + 'RadEnab14/static'


# CTP integration
# Do CTP integration ?
CTP_DO_PIPELINE = True

# the directory where all of the anonymized (CTP) data is stored
CTP_DATA_DIR = '/mnt/bigdisk/data16/mike16/ctp/ctp_data'

# the directory where the CTP is installed
CTP_HOME = '/mnt/bigdisk/data16/mike16/ctp/CTP'

# the name of the CTP daemon
CTP_DAEMON_NAME = 'ctpd'

# We can automatically configure the project output directory
CTP_DO_SAMBA_CONFIG = True

# the name of the SAMBA daemon
CTP_SAMBA_DAEMON = 'smbd'

# the location of the samba configuration file
CTP_SAMBA_CONFIG_FILE = '/etc/samba/smb.conf'