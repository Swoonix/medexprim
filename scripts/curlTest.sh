#!/usr/bin/env bash
LOGIN_URL=http://127.0.0.1:7000/RadiomicsEnabler/login/
YOUR_USER='medexprim'
YOUR_PASS='password'
COOKIES=cookies.txt
CURL_BIN="curl -c $COOKIES -b $COOKIES"
rm $COOKIES

echo -n "Django Auth: get login page cookies"
$CURL_BIN -o login1.html $LOGIN_URL
DJANGO_TOKEN="csrfmiddlewaretoken=$(grep csrfmiddlewaretoken login1.html | awk 'BEGIN { FS="'\''"}; {print $6}' )"
#DJANGO_TOKEN="csrfmiddlewaretoken=$(grep csrftoken $COOKIES | awk '{print $7}' )"

echo -n " perform login ..."
$CURL_BIN -d "$DJANGO_TOKEN&username=$YOUR_USER&password=$YOUR_PASS" -X POST -o login2.html $LOGIN_URL

echo -n " get query page ..."
$CURL_BIN -o query1.html http://127.0.0.1:7000/RadiomicsEnabler/Batch/91/query/add/
DJANGO_TOKEN="csrfmiddlewaretoken=$(grep csrfmiddlewaretoken query1.html | awk 'BEGIN { FS="'\''"}; {print $6}' )"

echo -n " make query"
$CURL_BIN -L -F "$DJANGO_TOKEN" -F 'patient_last_name=Smith' -F 'study_date_from=2017-01-02' -o results.html  http://127.0.0.1:7000/RadiomicsEnabler/Batch/91/query/add/
    

#echo " logout"
#rm $COOKIES
