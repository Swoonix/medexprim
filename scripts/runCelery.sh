#!/usr/bin/env bash

export rootDir=/opt/RadEnab/site
xterm -hold -e 'cd ${rootDir} && celery -A sitebase worker -l info' &


