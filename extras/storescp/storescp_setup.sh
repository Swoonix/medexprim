#!/usr/bin/env bash

cd /etc/init.d
sudo cp /opt/RadEnab/extras/storescp/storescp .
sudo chmod a+xr storescp
sudo mkdir /var/log/storescp
sudo chmod 02755 /var/log/storescp
sudo chown radenab:radenab /var/log/storescp
sudo mkdir /var/run/storescp
sudo chmod 02755 /var/run/storescp
sudo chown radenab:radenab /var/run/storescp
sudo cp /opt/RadEnab/extras/storescp/storescplog.cfg /etc/default
sudo /etc/init.d/storescp start
sudo update-rc.d storescp defaults
sudo update-rc.d storescp enable