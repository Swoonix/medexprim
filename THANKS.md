# Radiomics Enabler® - Unleash the potential of your medical images archives (PACS) for research #

## Contributors ##

We would like to thank :

* the [CHU de Toulouse](http://www.chu-toulouse.fr), and especially Pr. Pierre PAYOUX, for his continuous support and feedback,
* the engineering school [ISIS](http://isis.univ-jfc.fr/) (Informatique et Système d'Information de Santé) and its past students Guillaume Serres, Jennifer Ayme, François Cabarrou, Antoine Lapeyre, and Xavier Schmoor, who wrote earlier version of Radiomics Enabler®,
* authors and contributors of the following open-source projects :

	- [Django project](https://www.djangoproject.com/), 
	- [Celery project](http://www.celeryproject.org), 
	- [dcmtk](http://dicom.offis.de) DICOM library.
