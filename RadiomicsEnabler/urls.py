# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.views.generic.base import RedirectView

from . import views

urlpatterns = [
    url(r'^favicon.ico$', RedirectView.as_view(url='/static/RadiomicsEnabler/favicon/favicon.ico')),
    url(r'^$', auth_views.LoginView.as_view(template_name='RadiomicsEnabler/login.html'), name='home'),
    url(r'^login/$', auth_views.LoginView.as_view( template_name='RadiomicsEnabler/login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view( template_name='RadiomicsEnabler/logout.html'), name='logout'),
    url(r'^Projects$', views.ProjectListView.as_view(), name='projectList'),
    url(r'^Project/(?P<pk>[0-9]+)/$', views.ProjectDetailView,name='projectDetails'),
    url(r'^Project/add/$', views.ProjectCreate.as_view(), name='projectCreate'),
    url(r'^Project/(?P<pk>[0-9]+)/archive/$', views.ProjectArchive.as_view(), name='projectArchive'),
    url(r'^Project/(?P<pk>[0-9]+)/modify/$', views.ProjectModify.as_view(), name='projectModify'),
    url(r'^Project/(?P<pk>[0-9]+)/add/$', views.BatchCreate.as_view(), name='batchCreate'),
    url(r'^Batch/(?P<pk>[0-9]+)/archive/$', views.BatchArchive.as_view(), name='batchArchive'),
    url(r'^Batch/(?P<pk>[0-9]+)/validate/$', views.BatchValidate.as_view(), name='batchValidate'),
    url(r'^Batch/(?P<pk>[0-9]+)/modify/$', views.BatchModify.as_view(), name='batchModify'),
    url(r'^Batch/(?P<pk>[0-9]+)/$', views.BatchDetail.as_view(), name='batchDetail'),
    url(r'^Batch/(?P<pk>[0-9]+)/query/add/$', views.BatchQueryCreate.as_view(), name='batchQueryCreate'),
    url(r'^Batch/(?P<bpk>[0-9]+)/Query/(?P<pk>[0-9]+)/detail/$', views.QueryResults.as_view(), name='queryResults'),
    url(r'^Batch/(?P<bpk>[0-9]+)/Query/(?P<pk>[0-9]+)/series/$', views.SeriesResults.as_view(), name='seriesDetail'),
    url(r'^Batch/(?P<pk>[0-9]+)/Extraction/(?P<uuid>[a-z0-9-]+)/$', views.ExtractionProgress.as_view(), name='extractionProgress'),
    url(r'^Study/(?P<pk>[0-9]+)/archive/$', views.StudyArchive.as_view(), name='studyArchive'),
    url(r'^Series/(?P<pk>[0-9]+)/archive/$', views.SeriesArchive.as_view(), name='seriesArchive'),
    url(r'^ListQuery/$', views.ListQuery, name='listQuery'),
    url(r'^ListQuery/(?P<pk>[0-9]+)/modify/$', views.SearchListQueryModify.as_view(), name='listQueryModify' ),
    url(r'^Search/(?P<pk>[0-9]+)/Extraction/$', views.SearchProgress.as_view(),name='searchProgress'),
    url(r'^Search/(?P<pk>[0-9]+)/Series/$', views.SeriesSearchProgress.as_view(),name='seriesSearchProgress'),
]