# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

from django.http import JsonResponse,HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.utils import timezone
from .tables import ProjectTable, BatchWaitingTable, BatchProcessingTable, BatchValidatedTable
from .models import Project, Batch, Search, Study, Series, StudyDataFile, SeriesDataFile, Profile
import json
from .forms import QueryForm, BatchExtractForm, ListQueryForm
from . import tasks
from django.utils.text import slugify
import shutil
import os.path
from django.shortcuts import render
from django_tables2 import RequestConfig
from sitebase.celery_task import app
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from . import cleaner
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth import authenticate, login
import tempfile
import django.middleware.csrf
from .forms import BatchForm,SearchForm
from .ctp_utils import ctp_utils

from django.views.generic.edit import FormView

def json_date_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    else:
        raise TypeError

def set_default_page_length_title():
    values = getattr(settings, 'RAD_ENAB_PAGE_LENGTHS', [-1,25, 100, 50, 150])
    listValues = json.dumps( values  )
    titles = getattr(settings, 'RAD_ENAB_PAGE_TITLES', ["All",25, 100, 50, 150] )
    listTitles = json.dumps( titles )
    return listValues, listTitles

@login_required(login_url= reverse_lazy('login') )
def ProjectListViewOld(request):
    profile = Profile.objects.get( user = request.user )
    userProjects = profile.project.all().exclude( archived = True )
    table = ProjectTable( userProjects, order_by="-modification_date")
    RequestConfig(request).configure(table)
    return render( request, 'RadiomicsEnabler/projectTableList.html', {'table':table})


class ProjectListView(LoginRequiredMixin,TemplateView):
    template_name = 'RadiomicsEnabler/projectTableList.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        profile = Profile.objects.get(user=self.request.user)
        profile.agreement = True
        profile.save()
        return reverse_lazy( 'projectList' )

    def post(self, request, *args, **kwargs):
        decision = request.POST.get('agreement')
        if 'accept' in decision:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return HttpResponseRedirect( reverse_lazy('login') )

    def get(self, request, *args, **kwargs):
        profile = Profile.objects.get(user=request.user)
        userProjects = profile.project.all().exclude(archived=True)
        table = ProjectTable(userProjects, order_by="-modification_date")
        RequestConfig(request).configure(table)
        return render(request, 'RadiomicsEnabler/projectTableList.html', {'table': table, 'agreement':profile.agreement})



class ProjectCreate(LoginRequiredMixin,CreateView):
    model = Project
    fields = [ 'name', 'description', 'approval_info']
    login_url = reverse_lazy('login')

    def get_success_url(self):
        profile = Profile.objects.get( user = self.request.user )
        profile.project.add( self.object )
        return reverse_lazy( 'projectDetails', kwargs={ 'pk':self.object.pk})

    def post(self, request, *args, **kwargs):
        if "_cancel" in request.POST:
            return HttpResponseRedirect(reverse_lazy( 'projectList' ))
        else:
            return super(ProjectCreate, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        slugname = slugify(form.instance.name, allow_unicode=True )
        for project in Project.objects.all():
            if slugname == slugify(project.name, allow_unicode=True ):
                form.add_error('name', 'Name needs to be unique if interpreted in all capital letters (ie {:s} vs {:s})'.format(form.instance.name,project.name))
                return super(ProjectCreate, self).form_invalid(form)
        form.instance.creation_date = timezone.now()
        form.instance.modification_date = timezone.now()
        form.instance.archived = False
        form.instance.owner = self.request.user

        if getattr( settings, 'CTP_DO_PIPELINE', False ):
            ctp_base_dir = getattr( settings, 'CTP_HOME', '/opt/ctp')
            ctp_data_dir = getattr( settings, 'CTP_DATA_DIR', '/data/CTP/')
            pacs_data_dir = getattr(settings, 'RAD_ENAB_DATA_DIR', '/opt/RadEnab/data' )
            ctp_daemon =  getattr(settings, 'CTP_DAEMON_NAME', 'ctpd' )
            samba_daemon = getattr(settings,'CTP_SAMBA_DAEMON', 'smbd' )
            samba_config_file = getattr(settings,  'CTP_SAMBA_CONFIG_FILE',  '/etc/samba/smb.conf' )
            ctp = ctp_utils(ctp_base_dir, ctp_data_dir, pacs_data_dir, ctp_daemon, samba_daemon, samba_config_file )
            ctp.add_project_as_thread( slugname )

        return super(ProjectCreate, self ).form_valid(form)


class ProjectModify(LoginRequiredMixin,UpdateView):
    model = Project
    fields = [ 'description', 'approval_info', 'owner' ]
    template_name = 'RadiomicsEnabler/project_modify.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        return reverse_lazy( 'projectList' )

    def post(self, request, *args, **kwargs):
        if "_cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(ProjectModify, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        form.instance.modification_date = timezone.now()
        form.instance.owner = self.request.user.username
        return super(ProjectModify, self ).form_valid(form)

class ProjectArchive(LoginRequiredMixin, UpdateView ):
    model = Project
    template_name = 'RadiomicsEnabler/project_confirm_delete.html'
    fields = ['archived']
    login_url = reverse_lazy('login')
    def get_success_url(self):
        return reverse_lazy( 'projectList' )

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(ProjectArchive, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        batchList = cleaner.clean_project( form.instance.pk )
        for bpk in batchList:
            batch = Batch.objects.get( pk = bpk )
            batch.archived = True
            batch.save()
        form.instance.archived = True
        form.instance.modification_date = timezone.now()
        return super(ProjectArchive, self).form_valid(form)

@login_required(login_url= reverse_lazy('login'))
def ProjectDetailView(request, pk):
    project = Project.objects.get( pk = pk )

    # all of the active batch objects associated with the project
    qset = Batch.objects.filter( project = project ).exclude( archived = True )

    # get the most recent one and update the modification time of the project
    # Need to manage none values, so this is more complicated than creating a list
    latestList = []
    latestExtract = qset.exclude( extraction_datetime = None)
    if latestExtract:
        latestList.append(latestExtract.latest('extraction_datetime' ).extraction_datetime )
    latestValid = qset.exclude( validation_datetime = None)
    if latestValid:
        latestList.append(latestValid.latest('validation_datetime' ).validation_datetime)
    latestScheduled = qset.exclude( scheduled_datetime = None)
    if latestScheduled:
        latestList.append(latestScheduled.latest('scheduled_datetime' ).scheduled_datetime)
    latestPreparation = qset.exclude( preparation_datetime = None)
    if latestPreparation:
        latestList.append(latestPreparation.latest('preparation_datetime' ).preparation_datetime)
    if latestList:
        project.modification_date = max( latestList )
        project.save()


    # get the batches associated with the preparation table
    qsetWaiting = qset.filter( status = Batch.W )
    # if we have some valid batches then iterate through them to update their
    # patient counts and study counts
    if qsetWaiting:
        for batch in qsetWaiting:
            studies = Study.objects.exclude(archived=True).filter( search__batch = batch ).filter(selected=True)
            batch.number_of_studies = studies.count()
            batch.number_of_patients =  studies.values_list('patient_id', flat=True).distinct().count()
            series = Series.objects.exclude(archived=True).filter(study__search__batch=batch).filter(selected=True)
            batch.number_of_series = series.count()
            batch.save()
    tableWaiting = BatchWaitingTable( qsetWaiting  )
    qsetProcessing = qset.exclude( status = Batch.W ).exclude( status = Batch.V)
    if qsetProcessing:
        for batch in qsetProcessing:
            studies = Study.objects.exclude(archived=True).filter(search__batch=batch).filter(selected=True)
            batch.number_of_studies = studies.count()
            batch.number_of_patients =studies.values_list('patient_id', flat=True).distinct().count()
            series = Series.objects.exclude(archived=True).filter(study__search__batch=batch).filter(selected=True)
            batch.number_of_series = series.count()
            seriesImages = SeriesDataFile.objects.exclude(archived=True).exclude(deleted=True).filter( series__study__search__batch = batch)
            studyImages = StudyDataFile.objects.exclude(archived=True).exclude(deleted=True).filter( study__search__batch = batch)
            batch.number_of_images = seriesImages.count() + studyImages.count()
            batch.size_in_bytes = float( sum(seriesImages.values_list('fileSize', flat=True))+ sum(studyImages.values_list('fileSize', flat=True)) )

            # get status, if the batch wasn't interrupted
            if ( not batch.status == Batch.I ):
                hasError = studies.filter( status = Study.ER ).count() > 0 or series.filter( status = Series.ER ).count() > 0
                hasPending = studies.filter( status = Study.PE ).count() > 0 or series.filter( status = Series.PE ).count() > 0
                hasComplete = studies.filter( status = Study.CO ).count() > 0 or series.filter( status = Series.CO ).count() > 0
                # set batch status accordingly
                if hasError:
                    batch.status = Batch.E
                if not hasError and hasPending and not hasComplete:
                    batch.status = Batch.S
                if not hasError and hasPending and hasComplete:
                    batch.status = Batch.P
                if not hasError and not hasPending and hasComplete:
                    batch.status = Batch.C
            batch.save()
    tableProcessing = BatchProcessingTable( qsetProcessing )

    qsetValidated =  qset.filter( status = Batch.V )
    if qsetValidated:
        for batch in qsetValidated:
            studies = Study.objects.exclude(archived=True).filter(search__batch=batch).filter(selected=True)
            batch.number_of_studies = studies.count()
            batch.number_of_patients =studies.values_list('patient_id', flat=True).distinct().count()
            series = Series.objects.exclude(archived=True).filter(study__search__batch=batch).filter(selected=True)
            batch.number_of_series = series.count()
            seriesImages = SeriesDataFile.objects.exclude(archived=True).exclude(deleted=True).filter( series__study__search__batch = batch)
            studyImages = StudyDataFile.objects.exclude(archived=True).exclude(deleted=True).filter( study__search__batch = batch)
            batch.number_of_images = seriesImages.count() + studyImages.count()
            batch.save()
    tableValidated = BatchValidatedTable( qsetValidated )
    config=RequestConfig(request)
    config.configure(tableWaiting)
    config.configure(tableProcessing)
    config.configure(tableValidated)
    return render( request, 'RadiomicsEnabler/project_detail.html', {'object':project,'tableWaiting':tableWaiting,'tableProcessing':tableProcessing,'tableValidated':tableValidated} )

class BatchCreate(LoginRequiredMixin,CreateView):
    model = Batch
    form_class = BatchForm
    login_url = reverse_lazy('login')

    def get_success_url(self):
        return reverse_lazy( 'batchQueryCreate', kwargs={ 'pk':self.object.pk})

    def post(self, request, *args, **kwargs):
        if "_cancel" in request.POST:
            url = reverse_lazy( 'projectDetails', kwargs={ 'pk':self.kwargs['pk']})
            return HttpResponseRedirect(url)
        else:

            return super(BatchCreate, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        # Verify that the batch name (used as
        project = Project.objects.get(pk=self.kwargs['pk'])
        slugname = slugify(form.instance.name, allow_unicode=True )
        for batch in Batch.objects.filter(project=project):
            if batch.name == form.instance.name:
                form.add_error('name', 'The batch name needs to be unique, {:s} is already defined for the project {:s}'.format(batch.name, project.name) )
                return super(BatchCreate, self).form_invalid(form)
            if slugname == slugify(batch.name, allow_unicode=True ):
                form.add_error('name', 'Name needs to be unique if interpreted in all capital letters (ie {:s} vs {:s})'.format(form.instance.name,batch.name))
                return super(BatchCreate, self).form_invalid(form)

        form.instance.preparation_datetime = timezone.now()
        form.instance.archived = False
        form.instance.project = Project.objects.get( pk = self.kwargs['pk'] )
        return super(BatchCreate, self ).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BatchCreate, self).get_context_data(**kwargs)
        context['project'] = Project.objects.get( pk = self.kwargs['pk'] )
        return context


class BatchArchive(LoginRequiredMixin, UpdateView ):
    model = Batch
    login_url = reverse_lazy('login')
    def get_success_url(self):
        batch = Batch.objects.get( pk = self.kwargs['pk'] )
        return reverse_lazy( 'projectDetails', kwargs={ 'pk': batch.project.pk})

    template_name = 'RadiomicsEnabler/batch_confirm_delete.html'
    fields = ['archived']

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(BatchArchive, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        cleaner.clean_batch( form.instance.pk)
        form.instance.archived = True
        return super(BatchArchive, self).form_valid(form)

class BatchModify(LoginRequiredMixin,UpdateView):
    model = Batch
    fields = [ 'name', 'description', ]
    template_name = 'RadiomicsEnabler/batch_modify.html'
    login_url = reverse_lazy('login')
    def get_success_url(self):
        batch = Batch.objects.get(pk=self.kwargs['pk'])
        project = Project.objects.get( batch=batch )
        return reverse_lazy( 'projectDetails', kwargs={ 'pk':project.pk})

    def post(self, request, *args, **kwargs):
        if "_cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(BatchModify, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        # Verify that the batch name (used as
        project = self.object.project
        slugname = slugify(form.instance.name, allow_unicode=True )
        for batch in Batch.objects.filter(project=project).exclude(name=self.object.name):
             if slugname == slugify(batch.name, allow_unicode=True ):
                form.add_error('name', 'Name needs to be unique if interpreted in all capital letters (ie {:s} vs {:s})'.format(form.instance.name,batch.name))
                return super(BatchModify, self).form_invalid(form)

        form.instance.preparation_datetime = timezone.now()
        form.instance.archived = False
        form.instance.project = project
        return super(BatchModify, self ).form_valid(form)


class BatchValidate(LoginRequiredMixin,UpdateView):
    model = Batch
    fields = ['status']
    template_name = 'RadiomicsEnabler/batch_confirm_validate.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        batch = Batch.objects.get( pk = self.kwargs['pk'] )
        return reverse_lazy( 'projectDetails', kwargs={ 'pk': batch.project.pk})

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            batch = Batch.objects.get(pk=self.kwargs['pk'])
            for filedata in StudyDataFile.objects.filter(study__search__batch=batch):
                filedata.deleted = True
                filedata.save()
            for filedata in SeriesDataFile.objects.filter(series__study__search__batch=batch):
                filedata.deleted = True
                filedata.save()

            medexprimDataDir = getattr(settings, 'RAD_ENAB_DATA_DIR', '/data/projects')
            batchName = slugify(batch.name)
            projName = slugify(batch.project.name)
            batchDir = os.path.join(medexprimDataDir, projName, batchName)
            shutil.rmtree(batchDir, ignore_errors=True)
            batch.status = Batch.V
            batch.validation_datetime = timezone.now()
            batch.save()
            url = self.get_success_url()
            return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super(BatchValidate, self).get_context_data(**kwargs)
        batch = Batch.objects.get(pk=self.kwargs['pk'])
        context['object'] = batch
        return context

class BatchDetail(LoginRequiredMixin,DetailView):
    model = Batch
    template_name = 'RadiomicsEnabler/batch_detail.html'
    form_class = BatchExtractForm
    login_url = reverse_lazy('login')

    def post(self, request, *args, **kwargs):
        if "extract" in request.POST:
            # need to save the group ids of the child tasks to be able to delete the jobs if necessary
            groupResult = tasks.extractAllImages( kwargs['pk'] )
            batch = Batch.objects.get( pk = kwargs['pk'] )
            batch.extractions_running = True
            batch.taskIds = ','.join( [ iTask.id for iTask in groupResult.children ] )
            batch.save(update_fields=['taskIds','extractions_running'])
            url = reverse_lazy('extractionProgress', kwargs={'pk': self.kwargs['pk'], 'uuid': groupResult.id})
            return HttpResponseRedirect(url)
        elif "validate" in request.POST:
            url = reverse_lazy('batchValidate', kwargs={'pk': self.kwargs['pk']})
            return HttpResponseRedirect(url)
        elif "retry" in request.POST:
            groupResult = tasks.retryExtraction( kwargs['pk']  )
            batch = Batch.objects.get( pk = kwargs['pk'] )
            # Normally if we are here there are some studies/series that need to be recovered from the PACS
            # however, by chance, if all the studies are complete, then we return to the batch detail page
            if len( groupResult.children ) > 0:
                batch.taskIds = ','.join( [ iTask.id for iTask in groupResult.children ] )
                batch.save(update_fields=['taskIds'])
                url = reverse_lazy('extractionProgress', kwargs={'pk': self.kwargs['pk'], 'uuid': groupResult.id})
            else:
                updateToCompleteBatch(batch)
                batch.save()
                url = reverse_lazy('batchDetail', kwargs={'pk': self.kwargs['pk']})

            return HttpResponseRedirect(url)

    def get(self, request, *args, **kwargs):
        # Check to make sure that the batch has some entries if its status is W or 'Waiting to be Extracted",
        # otherwise send to Query definition page
        batch = Batch.objects.get(pk=kwargs['pk'])
        hasQueries = True
        if batch.status == Batch.W:
            hasQueries = False
            queries = Search.objects.filter(batch=batch).exclude(archived=True)
            for query in queries:
                studies = Study.objects.filter(search=query).exclude(archived=True).filter(selected=True)
                if studies:
                    hasQueries = True
                    break
        # If we have queries, show the batch elements
        if hasQueries:
            response = super(BatchDetail, self).get(request, *args, **kwargs)
            return response
        else:
            url = reverse_lazy('batchQueryCreate', kwargs={'pk': self.kwargs['pk']})
            return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super(BatchDetail, self).get_context_data(**kwargs)
        pageLength, pageTitle = set_default_page_length_title()
        context['pageLength'] = pageLength
        context['pageTitle'] = pageTitle
        queries = Search.objects.filter( batch = self.object ).exclude( archived = True )
        dispData = []
        seriesStatus = dict( Series.status_choices )
        studyStatus = dict( Study.status_choices )
        for query in queries:
            studies = Study.objects.filter(search=query).exclude(archived=True).filter(selected=True)
            for study in studies:
                # have to analyse if all the associated series data are complete (or not)
                series = Series.objects.filter( study = study ).exclude( archived = True ).filter(selected=True)
                if series:
                    if series.filter( status = Series.ER ):
                        study.status = Study.ER
                    if series.filter( status = Series.PE ):
                        study.status = Study.PE
                    if series.filter( status = Series.CO).count() == series.count():
                        study.status = Study.CO
                    study.save()
                styledStudyStatus = studyStatus[study.status] # '<P class="'+ studyStatus[study.status].lower() + '">' + studyStatus[study.status] + "</P>"
                studyDisplayData = { 'patient_id':study.patient_id, 'patient_last_name':study.patient_last_name, 'patient_first_name':study.patient_first_name,'patient_birth_date':study.patient_birth_date,'study_date':study.study_date, 'event_date':study.event_date,'modality':study.modality,'study_description':study.study_description,'selected': 0,'series':[],'pk':'Study/'+str(study.pk),'status':styledStudyStatus,'studyid':study.dicom_uid}
                dispData.append( studyDisplayData )

                for serie in series:
                    styledSerieStatus = seriesStatus[serie.status]  #'<P class="'+ seriesStatus[serie.status].lower() + '">' + seriesStatus[serie.status] + "</P>"
                    seriesDisplayData = {'patient_id':study.patient_id, 'patient_last_name':study.patient_last_name,
                                         'patient_first_name':study.patient_first_name,'patient_birth_date':study.patient_birth_date,
                                         'study_date':study.study_date,
                                        'event_date': study.event_date, 'modality': serie.modality,
                                        'study_description': serie.series_description, 'selected': 0,
                                        'series': [], 'pk': 'Series/' + str(serie.pk), 'status': styledSerieStatus,
                                        'studyid': study.dicom_uid}
                    dispData.append(seriesDisplayData)

        if dispData:
            context['studies'] = dispData
            context['jsonStudies'] = json.dumps( dispData, default=json_date_handler )
        else:
            context['studies'] = None
            context['jsonStudies'] = json.dumps('', default=json_date_handler)
        return context


class BatchQueryCreate(LoginRequiredMixin,CreateView):
    model = Search
    template_name = 'RadiomicsEnabler/batch_create_query.html'
    form_class = QueryForm
    login_url = reverse_lazy('login')
    def get_success_url(self):
        return reverse_lazy('searchProgress', kwargs={'pk': self.object.pk})

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url=reverse_lazy('batchDetail', kwargs={'pk': self.kwargs['pk']})
            return HttpResponseRedirect(url)
        else:
            return super(BatchQueryCreate, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        form.instance.archived = False
        form.instance.batch = Batch.objects.get( pk = self.kwargs['pk'] )
        return super(BatchQueryCreate, self ).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BatchQueryCreate, self).get_context_data(**kwargs)
        context['batch'] = Batch.objects.get( pk = self.kwargs['pk'] )
        return context

class SearchListQueryModify(UpdateView):
    model = Search
    template_name = 'RadiomicsEnabler/search_list_query_modify.html'
    form_class = ListQueryForm
    login_url = reverse_lazy('login')

    def get_success_url(self):
        return reverse_lazy( 'searchProgress', kwargs={ 'pk': self.kwargs['pk'] })


    def dispatch(self, request, *args, **kwargs):
        return super(SearchListQueryModify, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url=reverse_lazy('logout')
            return HttpResponseRedirect(url)
        elif "validate" in request.POST:
            return super(SearchListQueryModify, self).post(request, *args, **kwargs)
        else:
            json_data = json.loads(request.body)
            username = json_data['username']
            password = json_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                print( request.session.session_key )
                response = super(SearchListQueryModify, self).get(request, *args, **kwargs);
                response.set_cookie( getattr( settings, 'SESSION_COOKIE_NAME'), request.session.session_key )
                return response

    def get(self, request, *args, **kwargs):
        response = super(SearchListQueryModify, self).get(request, *args, **kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(SearchListQueryModify, self).get_context_data(**kwargs)
        context['search'] = Search.objects.get( pk = self.kwargs['pk'] )
        return context


@ensure_csrf_cookie
def ListQuery(request):

    """
    View which treats a programmatic interface to perform a search on a loaded list of patients
    :param request: 
    """
    if request.method == 'POST':

        # Extract the elements from the POST request
        json_data = json.loads(request.body)

        # username and password for validity ...
        try:
            username = json_data['username']
            password = json_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                JsonResponse({'status': 0, 'mesg':"User or password error for {:s}:{:s}".format( username, password )})

        except KeyError:
            JsonResponse({'status': 0, 'mesg':"Bad request. No username and/or password fields in JSON data."})

        # get the project name from the
        project = None
        try:
            projectName = json_data['project']
            project = Project.objects.get( name = projectName )
            if project is None:
                JsonResponse({'status': 0, 'mesg':'No valid project with name {:s}'.format(projectName)})
        except KeyError:
            JsonResponse({'status': 0, 'mesg':'Bad request. No project field in JSON data.'})

        # element used to name the temporary file where data is stored
        # this element is optional
        batchBaseName = 'listQuery'
        if 'batch' in json_data.keys():
            batchBaseName = json_data['batch']


        # input the patient list file, and copy it to the media directory
        patientListFile = None
        try:
            patientList = json_data['patientList']
            if len( patientList ) < 1:
                JsonResponse({'status': 0, 'mesg': 'Bad request. Patient list is empty.'})
            try:
                text_file = tempfile.NamedTemporaryFile( dir = settings.MEDIA_ROOT, mode='w', prefix=batchBaseName, delete=False )
                os.chmod( text_file.name, 0o666 )
                patientListFile = text_file.name
                for patientID in patientList:
                    text_file.write("{:s}\n".format(patientID))
                text_file.flush()
                text_file.close()
            except (RuntimeError, IOError ):
                JsonResponse({'status': 0, 'mesg': 'Problem creating temporary list file: {:s}.'.format( text_file.name)})

        except KeyError:
            JsonResponse({'status': 0, 'mesg':'Bad request. No patientList field in JSON data.'})

        # Create a new batch by adding the time
        refDate = timezone.now()
        batchName = batchBaseName + '_' + slugify(refDate).replace( '-','')[7:]
        if len( batchName ) > 25:
            batchName = batchName[0:25]
        batch = Batch( name=batchName, preparation_datetime = refDate, archived=False, project=project, description='List query' )
        batch.save()

        # create a new search object with the patient list file specified
        search = Search( batch = batch, patient_list = patientListFile )
        search.save()

        # pass the URL towards which the
        url = reverse_lazy('listQueryModify', kwargs={'pk': search.pk} )
        responseDict = { 'url': '{:s}'.format( url ) , 'status': 1 }
        response = JsonResponse(responseDict)
        return response


    elif request.method == 'GET':
        csrftoken = django.middleware.csrf.get_token(request)
        responseDict = {'csrftoken':csrftoken,'username':"username", 'password':"passwod", 'project':"project name (must exist)", 'patientList': ["List","of","patientNames",] }
        response = JsonResponse(responseDict)
        return response






class QueryResults(LoginRequiredMixin,FormView):
    template_name = 'RadiomicsEnabler/query_detail.html'
    #model = Search
    form_class = SearchForm
    login_url = reverse_lazy('login')

    def get_success_url(self):
        return reverse_lazy( 'batchDetail', kwargs={ 'pk': self.kwargs['bpk'] })

    def post(self, request, *args, **kwargs):

        # manually verify the form inputs are correct, if not, send an invalid form message
        rtrn = self.is_valid()
        if  len( rtrn ) > 0:
            context = super(QueryResults, self).get_context_data(**kwargs)
            form = context['form']
            form.add_error(None, rtrn )
            return super(QueryResults, self).form_invalid(form)

        if "goToBatch" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)

        elif "displaySelectedSeries" in request.POST:
            search = Search.objects.get(pk=self.kwargs['pk'])
            studyCnt = 0
            for key in request.POST.keys():
                if '_' in key:
                    vars = key.split("_")
                    if vars[0] == 'study':
                        studyCnt = studyCnt + 1
                        study = Study.objects.get( pk = vars[1])
                        study.doSeriesSearch = True
                        study.save()
            search.nSeriesSearch = studyCnt
            groupResult = tasks.runRemoteSeriesSearch( search.pk)
            search.seriesTaskIds = ','.join([iTask.id for iTask in groupResult.children])
            search.save(update_fields=['seriesTaskIds','nSeriesSearch'])
            url = reverse_lazy('seriesSearchProgress', kwargs={'pk': search.pk})
            return HttpResponseRedirect(url)

        elif "addSelectionToBatch" in request.POST:
            batch = Batch.objects.get( pk = self.kwargs['bpk'] )
            for key in request.POST.keys():
                if '_' in key :
                    vars = key.split("_")
                    if vars[0] == 'study':
                        studyObj = Study.objects.filter( pk = vars[1])[0]
                        if studyObj:
                            studyObj.selected = True
                            studyObj.batch = batch
                            studyObj.save()
                            print( studyObj )
                    elif vars[0] == "series":
                        seriesObj = Series.objects.get( pk = vars[1])
                        print( seriesObj )
                        if seriesObj:
                            studyObj = Study.objects.filter( series = seriesObj )[0]
                            print( studyObj )
                            if studyObj:
                                studyObj.selected = True
                                studyObj.batch = batch
                                studyObj.save()
                            seriesObj.selected = True
                            seriesObj.batch = batch
                            seriesObj.save()
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        elif "newQuery" in request.POST:
            url = reverse_lazy('batchQueryCreate', kwargs={'pk': self.kwargs['bpk']})
            return HttpResponseRedirect(url)
        else:
            return super(QueryResults, self).post(request, *args, **kwargs)

    def is_valid(self):
        rtrn = ''
        if "addSelectionToBatch" in self.request.POST:
            hasSelection = False
            for key in self.request.POST.keys():
                if 'study' in key or 'series' in key:
                    hasSelection = True
                    break
            if not hasSelection:
                rtrn = 'You must select at least one study or series to add to the batch.'

        if "displaySelectedSeries" in self.request.POST:
            hasSelection = False
            for key in self.request.POST.keys():
                if 'study' in key:
                    hasSelection = True
                    break
            if not hasSelection:
                rtrn = 'You must select at least one study to search for its related series data.'

        return rtrn


    def form_valid(self, form ):

        if "addSelectionToBatch" in self.request.POST:
            hasSelection = False
            for key in self.request.POST.keys():
                if 'study' in key or 'series' in key:
                    hasSelection = True
                    break
            if not hasSelection:
                form.add_error(None, 'You must select at least one study or series to add to the batch.')
                return super(QueryResults, self).form_invalid(form)

        if "displaySelectedSeries" in self.request.POST:
            hasSelection = False
            for key in self.request.POST.keys():
                if 'study' in key:
                    hasSelection = True
                    break
            if not hasSelection:
                form.add_error(None, 'You must select at least one study to search for its related series data.')
                return super(QueryResults, self).form_invalid(form)
        form.instance.archived = False
        form.instance.batch = Batch.objects.get( pk = self.kwargs['pk'] )
        return super(QueryResults, self  ).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(QueryResults, self).get_context_data(**kwargs)
        pageLength, pageTitle = set_default_page_length_title()
        context['pageLength'] = pageLength
        context['pageTitle'] = pageTitle
        # get the studies or run the dicom search
        search = Search.objects.get( pk = self.kwargs['pk'])
        context['object'] = search
        context['rtrnStatus'] = search.rtrnStatus
        studies = Study.objects.filter( search = search ).exclude( archived = True )

        #  accumulate the data for display
        dispData = []
        for study in studies:
            seriesDisplayData = []
            series = Series.objects.filter( study = study ).exclude( archived = True )
            for serie in series:
                sselected = 0
                if serie.selected:
                    sselected = 1
                seriesDisplayData.append( {"series_description":serie.series_description,"modality":serie.modality,"selected":sselected,"pk":serie.pk, 'serieid':serie.dicom_uid,'patient_id':study.patient_id})
            studySelected = 0
            if study.selected:
                studySelected = 1
            studyDisplayData = { 'patient_id':study.patient_id, 'patient_last_name':study.patient_last_name, 'patient_first_name':study.patient_first_name,'patient_birth_date':study.patient_birth_date,'study_date':study.study_date, 'event_date':study.event_date,'modality':study.modality,'study_description':study.study_description,'selected': studySelected,'series':seriesDisplayData,'pk':study.pk, 'studyid':study.dicom_uid}
            dispData.append( studyDisplayData )
        if dispData:
            context['studies'] = dispData
            context['jsonStudies'] = json.dumps( dispData, default=json_date_handler )
        else:
            context['studies'] = None
            context['jsonStudies'] = json.dumps( '', default=json_date_handler )

        context['doViewer'] = json.dumps( False, default=json_date_handler )
        if getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_USE', False ):
            context['doViewer'] = json.dumps(True, default=json_date_handler)
        context['patientIDLabel'] = json.dumps(getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_PATIENTIDLABEL', 'None'), default=json_date_handler)
        context[ 'studyUIDLabel' ] = json.dumps( getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_STUDYUIDLABEL','None'), default=json_date_handler)
        context[ 'seriesUIDLabel' ] = json.dumps( getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_SERIESUIDLABEL','None'), default=json_date_handler)
        context['baseURL'] = json.dumps( getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_BASEURL','None' ), default=json_date_handler)

        return context


class SeriesResults(LoginRequiredMixin,FormView):
    model = Search
    form_class = SearchForm
    template_name = 'RadiomicsEnabler/series_detail.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        return reverse_lazy( 'batchDetail', kwargs={ 'pk': self.kwargs['bpk'] })

    def post(self, request, *args, **kwargs):

        # manually verify the form inputs are correct, if not, send an invalid form message
        rtrn = self.is_valid()
        if  len( rtrn ) > 0:
            context = super(SeriesResults, self).get_context_data(**kwargs)
            form = context['form']
            form.add_error(None, rtrn )
            return super(SeriesResults, self).form_invalid(form)

        if "goToBatch" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)

        elif "addSelectionToBatch" in request.POST:
            batch = Batch.objects.get( pk = self.kwargs['bpk'] )
            for key in request.POST.keys():
                if '_' in key :
                    vars = key.split("_")
                    if vars[0] == 'study':
                        pass
                    elif vars[0] == "series":
                        seriesObj = Series.objects.get( pk = vars[1])
                        print( seriesObj )
                        if seriesObj:
                            seriesObj.selected = True
                            seriesObj.save()
                            study = Study.objects.get( series = seriesObj )
                            study.selected = True
                            study.save()
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        elif "newQuery" in request.POST:
            url = reverse_lazy('batchQueryCreate', kwargs={'pk': self.kwargs['bpk']})
            return HttpResponseRedirect(url)
        else:
            return super(SeriesResults, self).post(request, *args, **kwargs)

    def is_valid(self):
        rtrn = ''
        if "addSelectionToBatch" in self.request.POST:
            hasSelection = False
            for key in self.request.POST.keys():
                if 'study' in key or 'series' in key:
                    hasSelection = True
                    break
            if not hasSelection:
                rtrn = 'You must select at least one study or series to add to the batch.'
        return rtrn


    def get_context_data(self, **kwargs):
        context = super(SeriesResults, self).get_context_data(**kwargs)
        #  pagelength menu values
        pageLength, pageTitle = set_default_page_length_title()
        context['pageLength'] = pageLength
        context['pageTitle'] = pageTitle
        # get the studies or run the dicom search
        search = Search.objects.get( pk = self.kwargs['pk'])
        context['object'] = search
        studyList = Study.objects.filter( search = search ).exclude( archived = True ).filter( doSeriesSearch = True )

        #  accumulate the data for display
        dispData = []
        seriesCnt = 0
        errCnt = 0
        badStudyList = []
        for study in studyList:
            studySelected = 0
            seriesDisplayData = []
            series = Series.objects.filter( study = study ).exclude( archived = True )
            seriesCnt = seriesCnt + series.count()
            if series.count() ==0:
                errCnt = errCnt + 1
                badStudyList.append("Name: {:s} UID: {:s}".format( study.patient_last_name, study.dicom_uid))

            for serie in series:
                sselected = 0
                if serie.selected:
                    sselected = 1
                seriesDisplayData = {'patient_id': study.patient_id, 'patient_last_name': study.patient_last_name,
                                    'patient_first_name': study.patient_first_name,
                                    'patient_birth_date': study.patient_birth_date,
                                    'study_date': study.study_date, 'event_date': study.event_date,
                                    'modality': study.modality,'serie_modality': serie.modality,
                                    'study_description':study.study_description,'serie_description': serie.series_description, 'studyid': study.dicom_uid,
                                    'selected': sselected,'serieid':serie.dicom_uid,
                                    'pk': 'series_' + str(serie.pk), 'study_pk': 'study_' + str(study.pk)}
                dispData.append(seriesDisplayData)

        rtrnStatus = ''
        if errCnt == 0:
            rtrnStatus = "For {:d} selected studies found {:d} series.".format( search.nSeriesSearch, seriesCnt )
        else:
            rtrnStatus = "For {:d} selected studies, found no series data for {:d} studies.".format( search.nSeriesSearch, errCnt )
            rtrnStatus = rtrnStatus + "\nStudies with Errors:\n" + ";".join( badStudyList )
        context['rtrnStatus'] = rtrnStatus
        if dispData:
            context['studies'] = dispData
            context['jsonStudies'] = json.dumps( dispData, default=json_date_handler )
        else:
            context['studies'] = None
            context['jsonStudies'] = json.dumps('', default=json_date_handler)


        context['doViewer'] = json.dumps( 'False', default=json_date_handler )
        if getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_USE', False ):
            context['doViewer'] = json.dumps('True', default=json_date_handler)
        context['patientIDLabel'] = json.dumps(getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_PATIENTIDLABEL', 'None'), default=json_date_handler)
        context[ 'studyUIDLabel' ] = json.dumps( getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_STUDYUIDLABEL','None'), default=json_date_handler)
        context[ 'seriesUIDLabel' ] = json.dumps( getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_SERIESUIDLABEL','None'), default=json_date_handler)
        context['baseURL'] = json.dumps( getattr(settings, 'RAD_ENAB_EXTERNAL_VIEWER_BASEURL','None' ), default=json_date_handler)

        return context



class ExtractionProgress(LoginRequiredMixin,TemplateView):
    template_name = 'RadiomicsEnabler/extractionProgress.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        return reverse_lazy( 'batchDetail', kwargs={ 'pk': self.kwargs['pk'] })

    def post(self, request, *args, **kwargs):
        if "StopRetrieval" in request.POST:

            batch = Batch.objects.get(pk=self.kwargs['pk'])
            for uuid in batch.taskIds.split( ',' ):
                app.control.revoke(uuid, terminate=True )
            #from celery.result import GroupResult
            #GroupResult(self.kwargs['uuid']).revoke(terminate=True)
            #from celery import group
            #group( id)
            batch.taskIds = ''
            batch.extractions_running = False
            batch.status = Batch.I
            batch.extraction_datetime = None
            batch.save()
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        elif "ReturnToBatch" in request.POST:
            url = reverse_lazy('batchDetail', kwargs={'pk': self.kwargs['pk']})
            return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super(ExtractionProgress, self).get_context_data(**kwargs)
        batch = Batch.objects.get( pk = self.kwargs['pk'] )
        runStatus = 1
        if  batch.extractions_running == False:
            mesg = 'Extraction finished: {:d} with {:d} successful and {:d} errors'.format( batch.num_extractions, batch.num_extractions_success, batch.num_extractions_failure )
            runStatus = 0
        else:
            mesg = 'Extraction ongoing: {:d} with {:d} successful and {:d} errors'.format(batch.num_extractions,
                                                                                           batch.num_extractions_success,
                                                                                           batch.num_extractions_failure)
        context['progressMessage'] = mesg
        context['runStatus'] = runStatus
        context['bpk'] = self.kwargs['pk']
        context['uuid'] = self.kwargs['uuid']
        context['batch'] = batch
        return context


class SearchProgress(LoginRequiredMixin,TemplateView):
    template_name = 'RadiomicsEnabler/searchProgress.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        search = Search.objects.get(pk=self.kwargs['pk'])
        return reverse_lazy( 'queryResults', kwargs={ 'pk': search.pk, 'bpk': search.batch.pk })

    def get(self, request, *args, **kwargs ):
        search = Search.objects.get(pk=self.kwargs['pk'])
        # if we are finished we go to the results of the search, otherwise update
        if search.isFinished:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(SearchProgress, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        search = Search.objects.get(pk=self.kwargs['pk'])
        url = reverse_lazy('batchDetail', kwargs={'pk': search.batch.pk})
        # by default, nothing else other than stopping the search happens with a post.
        # We leave the If-Then for the case when other options might become available.
        if "StopSearch" in request.POST:
            for uuid in search.taskId.split( ',' ):
                app.control.revoke(uuid, terminate=True )
            search.taskId = ''
            search.save( update_fields=["taskId"] )
            return HttpResponseRedirect(url)
        else:
            return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super(SearchProgress, self).get_context_data(**kwargs)
        search = Search.objects.get( pk = self.kwargs['pk'] )

        # Useful for debugging without using celery
        #import dicomSearch
        #dicomSearch.runDicomStudySearch( search )
        celeryMesg = ''
        if search.notRun:
            taskResult = tasks.runRemoteSearch.delay(kwargs['pk'])
            search.taskId = taskResult.id
            search.notRun = False
            search.isFinished = False
            # if celery is not running, the list of task IDs is the empty string
            # and the search is in error
            search.save( update_fields=["taskId","notRun",'isFinished'])

        runStatus = 1
        if  search.isFinished:
            runStatus = 0
            mesg = 'Search finished with {:d} results. '.format( search.numberOfResults ) + celeryMesg
        else:
            mesg = 'Search is ongoing with {:d} current results. '.format( search.numberOfResults )

        context['progressMessage'] = mesg
        context['runStatus'] = runStatus
        context['search'] = search
        return context


class SeriesSearchProgress(LoginRequiredMixin,TemplateView):
    template_name = 'RadiomicsEnabler/seriesSearchProgress.html'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        search = Search.objects.get(pk=self.kwargs['pk'])
        batch = Batch.objects.filter( search = search )[0]
        url = reverse_lazy('seriesDetail', kwargs={'pk': self.kwargs['pk'], 'bpk': batch.pk })
        return HttpResponseRedirect(url)

    def get(self, request, *args, **kwargs ):
        search = Search.objects.get(pk=self.kwargs['pk'])
        # if we are finished we go to the results of the search, otherwise update
        studySeriesSearchList = Study.objects.filter(search=search).filter(doSeriesSearch=True)
        totalSeries = studySeriesSearchList.count()
        finishedSeries = studySeriesSearchList.filter(seriesSearchFinished = True ).count()
        if totalSeries == finishedSeries :
            return self.get_success_url()
        else:
            return super(SeriesSearchProgress, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        search = Search.objects.get(pk=self.kwargs['pk'])
        url = reverse_lazy('batchDetail', kwargs={'pk': search.batch.pk})
        # by default, nothing else other than stopping the search happens with a post.
        # We leave the If-Then for the case when other options might become available.
        if "StopSearch" in request.POST:
            search = Search.objects.get(pk=self.kwargs['pk'])
            url = reverse_lazy('batchDetail', kwargs={'pk': search.batch.pk})
            # by default, nothing else other than stopping the search happens with a post.
            # We leave the If-Then for the case when other options might become available.
            for uuid in search.seriesTaskIds.split(','):
                app.control.revoke(uuid, terminate=True)
            search.seriesTaskIds = ''
            search.save()
            return HttpResponseRedirect(url)
        else:
            return HttpResponseRedirect(url)

    def get_context_data(self, **kwargs):
        context = super(SeriesSearchProgress, self).get_context_data(**kwargs)
        search = Search.objects.get( pk = self.kwargs['pk'] )
        runStatus = 1
        studySeriesSearchList = Study.objects.filter(search=search).filter(doSeriesSearch=True)
        totalSeries = studySeriesSearchList.count()
        finishedSeries = studySeriesSearchList.filter(seriesSearchFinished = True ).count()
        startedSeries = studySeriesSearchList.filter(seriesSearchStarted = True ).count()
        if finishedSeries == totalSeries:
            runStatus = 0
            mesg = 'The series search finished for {:d} studies'.format( totalSeries )
        else:
            mesg = 'The series search is ongoing with {:d} studies analysed out of {:d}'.format( finishedSeries, totalSeries )

        context['progressMessage'] = mesg
        context['runStatus'] = runStatus
        context['search'] = search
        return context


def updateToCompleteBatch( batch ):
    '''
    This function changes a batch that was in error to a batch that is completed.
    It modifies the data assuming that

    :param batch: current batcn
    '''

    batch.extraction_datetime = timezone.now()
    batch.status = Batch.C
    batch.project.modification_date = batch.extraction_datetime.date()
    batch.project.save(update_fields=['modification_date'] )

class StudyArchive(LoginRequiredMixin, UpdateView ):
    model = Study
    template_name = 'RadiomicsEnabler/study_confirm_delete.html'
    fields = ['archived']
    login_url = reverse_lazy('login')

    def get_success_url(self):
        study = Study.objects.get( pk = self.kwargs['pk'] )
        batch = Batch.objects.get( search__study = study )
        return reverse_lazy( 'batchDetail', kwargs={ 'pk':batch.pk})

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(StudyArchive, self).post(request, *args, **kwargs)

    def form_valid(self, form ):
        cleaner.clean_study( self.object.pk )
        form.instance.archived = True
        form.instance.selected = False

        # we delete all the series associated with the, study
        for series in Series.objects.filter( study = self.object ).exclude( archived = True ):
            cleaner.clean_series( series.pk )
            series.archived = True
            series.save()

        # update the batch status if the series is in error, and it is the last invalid data, complete the study
        batch = Batch.objects.get(search__study =self.object)
        if self.object.status == Study.ER:
            batch.num_extractions_failure = batch.num_extractions_failure - 1
            batch.num_extractions = batch.num_extractions - 1
            if (batch.num_extractions_failure == 0) and (batch.num_extractions == batch.num_extractions_success):
                updateToCompleteBatch( batch )
            batch.save()

        return super(StudyArchive, self).form_valid(form)


class SeriesArchive(LoginRequiredMixin,UpdateView):
    model = Series
    template_name = 'RadiomicsEnabler/series_confirm_delete.html'
    fields = ['archived']
    login_url = reverse_lazy('login')

    def get_success_url(self):
        series = Series.objects.get(pk=self.kwargs['pk'])
        batch = Batch.objects.get(search__study__series=series)
        return reverse_lazy('batchDetail', kwargs={'pk': batch.pk})

    def post(self, request, *args, **kwargs):
        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)
        else:
            return super(SeriesArchive, self).post(request, *args, **kwargs)

    def form_valid(self, form):

        # if the associated study has no other Series, then we archive the study also, NB at this point the series data has
        # not been changed (the series database element is updated in the call to super)
        study = Study.objects.get( series = self.object )
        seriesList = Series.objects.filter(study=study).exclude(archived=True).filter(selected=True)
        if len( seriesList ) == 1:
            study.archived = True
            study.selected = False
            study.save()

        # update the batch status if the series is in error, and it is the last invalid data, complete the study
        batch = Batch.objects.get(search__study__series=self.object)
        if self.object.status == Series.ER:
            batch.num_extractions_failure = batch.num_extractions_failure - 1
            batch.num_extractions = batch.num_extractions - 1
            if (batch.num_extractions_failure == 0) and (batch.num_extractions == batch.num_extractions_success):
                updateToCompleteBatch( batch )
            batch.save()


        cleaner.clean_series(self.object.pk)
        form.instance.archived = True
        form.instance.selected = False

        return super(SeriesArchive, self).form_valid(form)