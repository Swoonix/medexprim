# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

import os
from .models import Batch,Project,StudyDataFile,SeriesDataFile,Study,Series
import shutil
from django.utils.text import slugify
from django.conf import settings


def clean_series( spk ):
    '''
    Remove the series data files associated with a particular series
    :param spk: the series object identifier
    '''
    series = Series.objects.get( pk = spk )

    for seriesDataFile in SeriesDataFile.objects.filter(series=series):
        seriesDataFile.fileSize = 0
        seriesDataFile.deleted = True
        os.remove( seriesDataFile.filename )
        seriesDataFile.save()

def clean_study( spk ):
    '''
    Remove the series data files associated with a particular series
    :param spk: the study object identifier
    '''
    study = Study.objects.get( pk = spk )

    for studyDataFile in StudyDataFile.objects.filter(study = study):
        studyDataFile.fileSize = 0
        studyDataFile.deleted = True
        os.remove( studyDataFile.filename )
        studyDataFile.save()


def clean_batch( bpk ):
    '''
    Clean a batch by removing all image files associated with the batch.
    :param bpk: the Batch object identifier 
    '''
    batch = Batch.objects.get(pk=bpk)

    medexprimDataDir = getattr(settings, 'RAD_ENAB_DATA_DIR', '/data/projects')
    batchName = slugify(batch.name)
    projName = slugify(batch.project.name)
    batchDir = os.path.join(medexprimDataDir, projName, batchName)
    shutil.rmtree(batchDir, ignore_errors=True)

    for filedata in StudyDataFile.objects.filter(study__search__batch=batch):
        filedata.fileSize = 0
        filedata.deleted = True
        filedata.save()
    for filedata in SeriesDataFile.objects.filter(series__study__search__batch=batch):
        filedata.fileSize = 0
        filedata.deleted = True
        filedata.save()


def clean_project( ppk ):
    '''
    Clean a project by removing the associated data files from all of the batch associated with the project.
    The list of ids of the associated batchs are returned to be updated.
    
    :param ppk: the project object identifier 
    :return: bpkList: a list of batchs to change their status following the clean operation.
    '''
    project = Project.objects.get(pk=ppk)

    medexprimDataDir = getattr(settings, 'RAD_ENAB_DATA_DIR', '/data/projects')
    projName = slugify(project.name)
    projDir = os.path.join( medexprimDataDir, projName)
    shutil.rmtree(projDir, ignore_errors=True)

    batchList = list()
    for batch in Batch.objects.filter( project = project ):
        clean_batch( batch.pk )
        batchList.append( batch.pk )

    return batchList
