# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com
import glob
from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.

from RadiomicsEnabler.models import Search, Batch, Project, Profile, Study
from RadiomicsEnabler import dicomSearch, tasks


class tasksIsDesiredTestCase(TestCase):
    def setUp(self):
        pass
    def test_is_desired(self):

        nofile = tasks.is_desired('file', 'series')
        assert( nofile == False )

        # Data from dicom.co.uk
        file_list = glob.glob('./testInputs/SeriesUID/*')
        for file_name in file_list:
            file_desired = tasks.is_desired(file_name, '1.2.276.0.7230010.3.1.3.296487680.1.1544194570.314134')
            print('{:s} is desired {}'.format(file_name, file_desired))
            if file_name[-2:] in '3635':
                assert( file_desired == True)
            else:
                assert( file_desired == False )


class dcmFileTestCase(TestCase):

    def setUp(self):
        project = Project()
        project.name = 'project_auto_test'
        project.save()
        batch = Batch()
        batch.name ='batch_auto_test'
        batch.project = project
        batch.save()
        search = Search()
        search.patient_first_name = 'John'
        search.batch = batch
        search.save()
        self.pk = search.pk

    def test_dcm_files(self):

        search = Search.objects.get(pk=self.pk)
        testInputs = glob.glob( './testInputs/*.dcm')
        for fileInput in testInputs:
            print( 'Testing {:s}'.format( fileInput ))
            rtrnCnt = dicomSearch.analyse_study_dicom_file_list(search, [fileInput])
            self.assertEqual(rtrnCnt, 1)

        studyList = Study.objects.filter( search = search)
        for study in studyList:
            print( study )
            print( 'description: {:s}'.format( study.study_description ) )
            print( 'study id: {:s}'.format( study.study_id) )


class FileInputTestCase(TestCase):

    def setUp(self):
        project = Project()
        project.name = 'project_auto_test'
        project.save()
        batch = Batch()
        batch.name ='batch_auto_test'
        batch.project = project
        batch.save()
        self.bpk = batch.pk
        self.username = 'test1'
        self.passwd = 'test1'
        user = User.objects.create_user(self.username, 'test1@thetest.com', self.passwd)
        user.save()
        profile = Profile.objects.get(user=user)
        profile.agreement = True
        profile.save()



    def test_search_file_input(self):
        c = Client()
        isLoggedIn = c.login(username=self.username, password=self.passwd )
        batch = Batch.objects.get( pk = self.bpk )

        url = '/RadiomicsEnabler/Batch/{:d}/query/add/'.format( self.bpk )

        testInputs = glob.glob( './testInputs/*.csv')

        for fileInput in testInputs:
            with open( fileInput ) as fp:
                response = c.post( url, {'patient_list':fp})
                searchList = Search.objects.filter(batch=batch)
                self.assertEqual( True, True )

class DicomSearchTestCase(TestCase):


    def setUp(self):
        project = Project()
        project.name = 'project_auto_test'
        project.save()
        batch = Batch()
        batch.name ='batch_auto_test'
        batch.project = project
        batch.save()
        search = Search()
        search.patient_first_name = 'John'
        search.batch = batch
        search.save()
        self.pk = search.pk


    def test_study_search(self):
        search = Search.objects.get( pk = self.pk )
        searchTask = dicomSearch.runDicomStudySearch( search )
        self.assertTrue( not searchTask is None  )


