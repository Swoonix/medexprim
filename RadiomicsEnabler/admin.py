# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

from django.contrib import admin

# Register your models here.
from .models import Project,Batch,Search,Study,Series,StudyDataFile,SeriesDataFile,Profile

admin.site.register( Project )
admin.site.register( Batch )
admin.site.register( Search )
admin.site.register( Study )
admin.site.register( Series )
admin.site.register( StudyDataFile )
admin.site.register( SeriesDataFile )
admin.site.register( Profile )