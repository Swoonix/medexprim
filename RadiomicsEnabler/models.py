# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.
class Destination( models.Model):
    name = models.CharField( max_length=50, verbose_name='DICOM Destination',unique=True)
    short_name = models.CharField( max_length=50 )
    port = models.IntegerField()
    aetitle = models.CharField(max_length=16, verbose_name="AE Title")
    ipaddress = models.GenericIPAddressField()
    def __str__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=50, verbose_name='Project Name',unique=True)
    creation_date = models.DateField(auto_now_add=True, verbose_name='Creation Date')
    modification_date = models.DateField(null = True, blank=True, verbose_name='Last Update')
    description = models.TextField(max_length=250, verbose_name='Description', blank=True )
    approval_info = models.TextField(max_length=250, verbose_name='Approval Information', blank=True )
    owner = models.CharField(max_length=50, verbose_name='Owner', blank=True, null=True )
    destination = models.ForeignKey( Destination, blank = True, null= True, on_delete=models.SET_NULL )
    archived = models.BooleanField( default=False, verbose_name = 'Archived' )
    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    project = models.ManyToManyField( Project, blank = True, verbose_name= 'Project'  )
    agreement = models.BooleanField( default=False, verbose_name='Signed Warning')
    def __str__(self):
        return self.user.get_username()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Batch(models.Model):
    S = 'S'
    D = 'D'
    P = 'P'
    E = 'E'
    W = 'W'
    V = 'V'
    C = 'C'
    I = 'I'
    status_choices=((I, 'Interrupted'),(V,'Validated'),(E,'Error'),(D,'Done'), (W,'Waiting to be extracted'), (S,'Scheduled'), (P,'Processing'), (C,'Completed') )
    name = models.CharField(max_length=25 )
    description = models.TextField(max_length=250, verbose_name='Description', blank=True )
    project = models.ForeignKey( Project, on_delete=models.CASCADE )
    status = models.CharField( max_length=1, choices = status_choices, default=W )
    destination = models.CharField(blank=True, max_length=150)
    extraction_datetime = models.DateTimeField( null = True, blank=True, verbose_name = 'Extracted on' )
    validation_datetime = models.DateTimeField( null = True, blank=True, verbose_name = 'Validated on' )
    scheduled_datetime = models.DateTimeField( null = True, blank=True, verbose_name = 'Scheduled Date' )
    preparation_datetime = models.DateTimeField( null = True, blank=True, verbose_name = 'Prepared on' )
    number_of_patients = models.IntegerField( default= 0, verbose_name = 'Number of Patients' )
    number_of_studies = models.IntegerField( default= 0, verbose_name = 'Number of Studies' )
    number_of_series = models.IntegerField( default= 0, verbose_name = 'Number of Series' )
    number_of_images = models.IntegerField( default= 0, verbose_name = 'Number of Images' )
    size_in_bytes = models.BigIntegerField( default= 0, verbose_name = 'File Size' )
    archived = models.BooleanField( default=False, verbose_name = 'Archived' )
    num_extractions = models.IntegerField( default= 0, verbose_name = 'Number of Extractions' )
    num_extractions_success =  models.IntegerField( default= 0, verbose_name = 'Number of Successful Extractions' )
    num_extractions_failure =  models.IntegerField( default= 0, verbose_name = 'Number of Extractions in Error' )
    extractions_running = models.BooleanField( default = False )
    taskIds = models.TextField( default='', blank=True, verbose_name='Task IDs')

    class Meta:
        unique_together = ('name','project')

    def __str__(self):
        return self.name + ' (' + self.project.name + ')'

class Search(models.Model):
    study_date_from = models.DateField( verbose_name="From", blank = True, null = True )
    study_date_to = models.DateField( verbose_name="To", blank = True, null = True )
    patient_last_name = models.CharField(max_length=50, blank = True, verbose_name="Patient's last name" )
    patient_first_name = models.CharField(max_length=50, blank = True, verbose_name="Patient's first name" )
    patient_birth_date = models.DateField( verbose_name="Birth date", blank = True, null = True )
    patient_id = models.CharField(max_length=64, verbose_name="Patient ID", blank=True)
    patient_list = models.FileField(verbose_name="Patient list", blank = True, null = True, upload_to='uploads/patient_lists/%Y/%m/%d/' )
    CR = 'CR'
    CT = 'CT'
    MR = 'MR'
    NM = 'NM'
    US = 'US'
    PT = 'PT'
    XA = 'XA'
    MG = 'MG'
    PX = 'PX'
    modality_choices = ( (CR,'CR: Computed Radiography'),
                         (CT,'CT: Computed Tomography'),
                         (MR,'MR: Magnetic Resonance'),
                         (NM,'NM: Nuclear Medicine'),
                         (US,'US: Ultrasound'),
                         (PT,'PT: Positron emission tomography (PET)'),
                         (XA, 'XA: X-Ray Angiography'),
                         (MG, 'MG: Mammography'),
                         (PX,'PX: Panoramic X-Ray') )
    modality = models.CharField(max_length=60, verbose_name="Modality type(s)", blank=True )
    exam_types = models.CharField(max_length=180, verbose_name = "Exam type(s)", blank = True)
    batch = models.ForeignKey( Batch, on_delete=models.CASCADE )
    archived = models.BooleanField( default=False, verbose_name = 'Archived' )
    notRun = models.BooleanField( default=True, verbose_name = 'Not run' )
    rtrnStatus = models.TextField(blank=True, verbose_name='Search Status')
    numberOfResults = models.IntegerField(default=0,verbose_name="Number of results")
    isFinished = models.BooleanField( default=False, verbose_name="Is finished")
    advanced_search = models.FileField(verbose_name="Advanced search", blank = True, null = True, upload_to='uploads/advanced_search/%Y/%m/%d/' )
    taskId = models.TextField( default='', blank=True, verbose_name='Search Task ID')
    nSeriesSearch = models.IntegerField(default=0)
    seriesTaskIds = models.TextField( default='', blank=True, verbose_name='Series Task IDs')

    def __str__(self):
        search_str = 'Search (Batch={:s})->'.format( str(self.batch) )
        if self.study_date_from:
            search_str = search_str + 'From:' + str( self.study_date_from)
        if self.study_date_to:
            search_str = search_str + ' To:' + str(self.study_date_to)
        if self.modality:
            search_str = search_str + ' Modality:' + self.modality
        if self.patient_last_name:
            search_str = search_str + ' Last name:' + self.patient_last_name
        if self.patient_first_name:
            search_str = search_str + ' First name:' + self.patient_first_name
        if self.patient_birth_date:
            search_str = search_str + ' Birth date:' + str( self.patient_birth_date )
        if self.patient_id:
            search_str = search_str + ' Patient ID:' + self.patient_id
        if self.patient_list:
            search_str = search_str + ' Patient List:' + str( self.patient_list )
        return search_str


class Study(models.Model):
    CO = 'C'
    PE = 'P'
    ER = 'E'
    SE = 'S'
    status_choices = ( (CO,'Complete'),
                        (PE,'Pending'),
                        (ER,'Error'),
                       (SE, 'Series'), )
    patient_id = models.CharField(max_length=64, verbose_name="Patient ID", blank=True)
    dicom_uid = models.CharField(max_length=64, verbose_name="DICOM ID", blank=True)
    study_id = models.CharField(max_length=64, verbose_name="Study ID", blank=True)
    patient_last_name = models.CharField(max_length=50, blank = True, verbose_name="Patient's last name" )
    patient_first_name = models.CharField(max_length=50, blank = True, verbose_name="Patient's first name" )
    patient_birth_date = models.DateField( verbose_name="Birth date", blank = True, null = True )
    study_date = models.DateField(verbose_name="Study date", blank = True, null = True )
    event_date = models.DateField(verbose_name="Event date", blank = True, null = True )
    modality = models.CharField(max_length=50, blank = True )
    study_description = models.CharField(max_length=64, blank = True, verbose_name='Study Description')
    search = models.ForeignKey( Search, on_delete=models.CASCADE, null=True )
    num_search = models.IntegerField(default=0)
    selected = models.BooleanField( default=False, verbose_name= "Selected")
    archived = models.BooleanField( default=False, verbose_name = 'Archived' )
    status = models.CharField( max_length=1, choices=status_choices, default=PE )
    doSeriesSearch = models.BooleanField( default=False, verbose_name = 'Do series search')
    numSeriesResults = models.IntegerField(default=0, verbose_name="Number of associated series")
    seriesSearchStarted = models.BooleanField( default=False, verbose_name = 'Series search started')
    seriesSearchFinished = models.BooleanField( default=False, verbose_name = 'Series search finished')

    def __str__(self):
        return "Study-> Name: " + self.patient_last_name + " Patient ID:" + self.patient_id +  " DICOM UID:" + self.dicom_uid

class Series(models.Model):
    CO = 'C'
    PE = 'P'
    ER = 'E'
    status_choices = ( (CO,'Complete'),
                         (PE,'Pending'),
                         (ER,'Error'), )
    study = models.ForeignKey( Study, on_delete=models.CASCADE)
    series_description = models.CharField( max_length= 64, blank=True, verbose_name="Series Description" )
    modality = models.CharField( max_length= 50 )
    selected = models.BooleanField(default=False, verbose_name="Selected")
    filename = models.FileField(null = True, blank=True)
    archived = models.BooleanField(default=False, verbose_name='Archived')
    dicom_uid = models.CharField(max_length=64, verbose_name="DICOM ID", blank=True)
    status = models.CharField( max_length=1, choices=status_choices, default=PE )
    def __str__(self):
        rtrn = "Series UID:" + self.dicom_uid
        if self.series_description:
            rtrn = rtrn + " Description:" + self.series_description
        return rtrn

class StudyDataFile( models.Model):
    filename = models.FilePathField( max_length=256, verbose_name="Study DICOM File", null = True, blank=True)
    fileSize = models.PositiveIntegerField( default=0)
    archived = models.BooleanField(default=False, verbose_name='Archived')
    study = models.ForeignKey( Study, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False, verbose_name='Deleted')
    def __str__(self):
        return "Study File-> {:s} size:{:d}".format( self.filename, self.fileSize )

class SeriesDataFile( models.Model):
    filename = models.FilePathField( max_length=256, verbose_name="Series DICOM File", null = True, blank=True)
    fileSize = models.PositiveIntegerField( default=0)
    archived = models.BooleanField(default=False, verbose_name='Archived')
    series = models.ForeignKey( Series, on_delete=models.CASCADE)
    deleted = models.BooleanField(default=False, verbose_name='Deleted')
    def __str__(self):
        return "Series File->{:s} size:{:d}".format( self.filename, self.fileSize )