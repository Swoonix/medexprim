# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-17 13:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0052_auto_20170725_2311'),
    ]

    operations = [
        migrations.AddField(
            model_name='search',
            name='isFinished',
            field=models.BooleanField(default=False, verbose_name='Is finished'),
        ),
        migrations.AddField(
            model_name='search',
            name='numberOfResults',
            field=models.IntegerField(default=0, verbose_name='Number of results'),
        ),
    ]
