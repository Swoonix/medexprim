# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-29 17:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0032_auto_20170322_1214'),
    ]

    operations = [
        migrations.RenameField(
            model_name='series',
            old_name='study_type',
            new_name='series_description',
        ),
    ]
