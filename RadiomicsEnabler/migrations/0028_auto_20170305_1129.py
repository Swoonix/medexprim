# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-05 10:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0027_auto_20170301_2140'),
    ]

    operations = [
        migrations.CreateModel(
            name='DicomDataFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('filename', models.FilePathField(null=True, verbose_name='DICOM Data File')),
                ('fileSize', models.PositiveIntegerField(default=0)),
                ('series', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RadiomicsEnabler.Series')),
                ('study', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RadiomicsEnabler.Study')),
            ],
        ),
        migrations.AddField(
            model_name='batch',
            name='extractions_running',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='batch',
            name='num_extractions',
            field=models.IntegerField(default=0, verbose_name='Number of Extractions'),
        ),
        migrations.AddField(
            model_name='batch',
            name='num_extractions_failure',
            field=models.IntegerField(default=0, verbose_name='Number of Extractions in Error'),
        ),
        migrations.AddField(
            model_name='batch',
            name='num_extractions_success',
            field=models.IntegerField(default=0, verbose_name='Number of Successful Extractions'),
        ),
    ]
