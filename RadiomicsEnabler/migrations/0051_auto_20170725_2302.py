# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-25 21:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0050_auto_20170523_2321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='batch',
            name='extraction_datetime',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Extracted on'),
        ),
    ]
