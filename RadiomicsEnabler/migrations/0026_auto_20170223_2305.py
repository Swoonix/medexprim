# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-23 22:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0025_auto_20170223_2243'),
    ]

    operations = [
        migrations.AlterField(
            model_name='search',
            name='patient_list',
            field=models.FileField(blank=True, null=True, upload_to='uploads/patient_lists/%Y/%m/%d/', verbose_name='Patient list'),
        ),
    ]
