# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-22 08:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0030_auto_20170321_2353'),
    ]

    operations = [
        migrations.CreateModel(
            name='SeriesDataFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('filename', models.FilePathField(max_length=256, null=True, verbose_name='Series DICOM File')),
                ('fileSize', models.PositiveIntegerField(default=0)),
                ('series', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RadiomicsEnabler.Series')),
            ],
        ),
        migrations.CreateModel(
            name='StudyDataFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('filename', models.FilePathField(max_length=256, null=True, verbose_name='Study DICOM File')),
                ('fileSize', models.PositiveIntegerField(default=0)),
                ('study', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='RadiomicsEnabler.Study')),
            ],
        ),
        migrations.RemoveField(
            model_name='dicomdatafile',
            name='series',
        ),
        migrations.RemoveField(
            model_name='dicomdatafile',
            name='study',
        ),
        migrations.DeleteModel(
            name='DicomDataFile',
        ),
    ]
