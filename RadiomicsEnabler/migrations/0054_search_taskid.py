# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-17 16:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0053_auto_20170917_1540'),
    ]

    operations = [
        migrations.AddField(
            model_name='search',
            name='taskId',
            field=models.TextField(blank=True, default='', verbose_name='Search Task ID'),
        ),
    ]
