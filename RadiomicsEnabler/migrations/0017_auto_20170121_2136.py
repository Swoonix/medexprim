# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-21 20:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0016_auto_20170121_1829'),
    ]

    operations = [
        migrations.RenameField(
            model_name='series',
            old_name='name',
            new_name='study_type',
        ),
        migrations.AddField(
            model_name='series',
            name='selected',
            field=models.BooleanField(default=False, verbose_name='Selected'),
        ),
        migrations.AddField(
            model_name='study',
            name='dicom_id',
            field=models.CharField(blank=True, max_length=64, verbose_name='DICOM ID'),
        ),
        migrations.AddField(
            model_name='study',
            name='selected',
            field=models.BooleanField(default=False, verbose_name='Selected'),
        ),
    ]
