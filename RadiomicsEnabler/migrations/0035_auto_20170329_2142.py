# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-29 19:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0034_auto_20170329_2142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='study',
            name='study_description',
            field=models.CharField(blank=True, max_length=64, verbose_name='Study Description'),
        ),
    ]
