# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-21 23:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('RadiomicsEnabler', '0022_auto_20170201_2217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='search',
            name='modality',
            field=models.CharField(blank=True, max_length=60, verbose_name='Modality type(s)'),
        ),
        migrations.AlterField(
            model_name='search',
            name='patient_id',
            field=models.CharField(blank=True, max_length=64, verbose_name='Patient ID'),
        ),
    ]
