# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

from __future__ import absolute_import, unicode_literals
from celery import shared_task, group, signature

from RadiomicsEnabler import dicomSearch
from .models import Study,Series,Batch,SeriesDataFile,StudyDataFile,Search
import os, shlex, subprocess
import time
from django.db import transaction
import glob
from shutil import move
from django.utils.text import slugify
from django.conf import settings
from django.utils import timezone
import pydicom

def getLocalWorkDir( studyUid):
    """
    Generate the local work directory name where the storescp daemon stores the recovered dicom data files.
    :param studyUid: the DICOM study UID 
    """
    return getattr(settings, 'RAD_ENAB_STORESCU_PREFIX', 'RE') + '_' + studyUid

def constructCommand( commandOptions, studyUid, batchName, projName, seriesUid=None ):
    """
    Construct the command cmove used to recover the data

    :param commandOptions: a string giving the 
    :param studyUid: 
    :param batchName: 
    :param projName: 
    :return: (args, workDir, stdoutFile, stderrFile)
    :rtype: (str list of arguments, the working directory, the name of the std file, the name of the stderr file )
    """
    medexprimAetID = getattr(settings, 'PACS_AE_TITLE', 'RADENAB' )
    medexprimDataDir = getattr(settings, 'RAD_ENAB_DATA_DIR', '/data/projects' )
    medexprimDicomServer = getattr(settings,'PACS_DICOM_SERVER', 'www.dicomserver.co.uk' )
    medexprimDicomPort = getattr(settings,'PACS_MOVE_SCP_PORT','104')
    medexprimAEtitle = getattr(settings, 'RAD_ENAB_AE_TITLE', 'RADENAB' )
    movescuCmd = getattr(settings, 'RAD_ENAB_MOVE_SCU', '/usr/bin/movescu' )

    workDir =  os.path.join( medexprimDataDir, projName, batchName, studyUid )
    try:
        os.makedirs(workDir)
    except OSError:
        if not os.path.isdir(workDir):
            raise

    basestr = movescuCmd + ' -v --aetitle ' + medexprimAEtitle + ' --call ' + medexprimAetID + commandOptions

    thisSrchStr = basestr + ' ' + medexprimDicomServer + ' ' + medexprimDicomPort

    # filenames incorporate the UID into the filename to allow debugging
    uidPart = studyUid
    if not seriesUid is None:
        uidPart = seriesUid

    # Dump the cmove command to a file
    cmoveFile = "cmove_" + uidPart + ".txt"
    with open(os.path.join(workDir, cmoveFile ), "w") as text_file:
        text_file.write("{:s}".format(thisSrchStr))
        text_file.flush()
        text_file.close()

    args = shlex.split(thisSrchStr)
    stdoutFile = os.path.join(workDir, 'stdout_' + uidPart + '.log')
    stderrFile = os.path.join(workDir, 'stderr_' + uidPart + '.log')
    return args, workDir, stdoutFile, stderrFile

def is_desired( srcFile, seriesUid ):
    # If seriesUid is None, then we are looking at a study extraction, we take all of the files
    if seriesUid is None:
        return True

    # if we are looking at a series extraction, make sure that the correct files are moved
    # in case we have competing series extractions happening

    # Initialise the file Series UID
    fileSeriesID = None
    # Try to read the Series UID from the DICOM file, if this fails return false
    try:
        with pydicom.dcmread( srcFile, stop_before_pixels=True,specific_tags= ['SeriesInstanceUID']) as ds:
            if 'SeriesInstanceUID' in ds:
                fileSeriesID = ds.SeriesInstanceUID
    except FileNotFoundError as e:
        print( str(e) )
        return False

    # Verify that we have actually read the series UID, otherwise return false
    if fileSeriesID is None:
        print('Did not find a Series UID for file {:s}'.format( srcFile ) )
        return False

    # Check the seriesUID (finally)
    if seriesUid in fileSeriesID:
        return True
    else:
        return False


def analyseResults( studyUid, batchName, projName, seriesUid = None ):
    medexprimWorkDir  = getattr(settings,'RAD_ENAB_WORK_DIR','/data/work' )
    medexprimIncomingDir= getattr(settings, 'RAD_ENAB_INCOMING_DIR','INCOMING')
    medexprimDataDir = getattr(settings, 'RAD_ENAB_DATA_DIR', '/data/projects' )

    incomingDir =   os.path.join(medexprimWorkDir,medexprimIncomingDir, getLocalWorkDir( studyUid) )
    destinationDir = os.path.join(medexprimDataDir, projName, batchName, studyUid )

    rtrnStatus = False
    overwrite = False
    inFileList = glob.glob(os.path.join(incomingDir, '*'))
    if len( inFileList ) == 0:
        return rtrnStatus, overwrite, [], []

    fileSizeList = []
    fileList = []
    for srcFile in inFileList:
        if is_desired( srcFile, seriesUid ):
            fileSizeList.append(os.path.getsize(srcFile))
            newFile = os.path.join(destinationDir, os.path.basename(srcFile))
            if os.path.isfile(newFile):
                overwrite = True
                os.remove( newFile )
            move( srcFile, destinationDir )
            fileList.append( newFile )
            rtrnStatus = True

    return rtrnStatus, overwrite, fileList, fileSizeList

@shared_task(bind=True )
def extractStudy( self, studyUid, batchName, batchPK, projName ):
    studyOptions = " -S -k 0020,000D=" + studyUid + " "  + '-k 0008,0052=STUDY'
    args, workDir, stdoutFileName, stderrFileName = constructCommand( studyOptions, studyUid, batchName, projName )
    stdoutFile = open( stdoutFileName, 'w')
    stderrFile = open( stderrFileName, 'w')

    # create the call
    s = 1
    try:
        s = subprocess.call( args, cwd=workDir, stdout=stdoutFile, stderr=stderrFile)
    except subprocess.CalledProcessError as e:
        print( str(e) )
    except Exception as e:
        print( 'Unknown error: {:s}'.format( str(e)))

    # if it returns did not work
    if s != 0:
        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.num_extractions_failure = batch.num_extractions_failure + 1
            batch.status = Batch.E
            # change the batch status if it is the last element of the batch and it is in error
            if (batch.num_extractions_failure + batch.num_extractions_success ) == batch.num_extractions:
                batch.extractions_running = False
            batch.save( update_fields=['num_extractions_failure','status','extractions_running'])
            study = Study.objects.select_for_update().filter( search__batch = batch ).filter( dicom_uid = studyUid ).filter( selected = True )[0]
            study.status = Study.ER
            study.save()
            return s

    # try and analyse the results
    rtrnStatus, overwrite, fileList, fileSizeList = analyseResults(studyUid, batchName, projName)

    # if the results were not good
    if not rtrnStatus:
        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.num_extractions_failure = batch.num_extractions_failure + 1
            batch.status = Batch.E
            # change the batch status if it is the last element of the batch and it is in error
            if (batch.num_extractions_failure + batch.num_extractions_success ) == batch.num_extractions:
                batch.extractions_running = False
            batch.save( update_fields=['num_extractions_failure','status','extractions_running'])
            study = Study.objects.select_for_update().filter(  search__batch = batch ).filter( dicom_uid = studyUid ).filter( selected = True )[0]
            study.status = Study.ER
            study.save()
            return 1

    # create the list of datafiles, save them, and update the batch
    else:

        batch = Batch.objects.get(pk=batchPK)
        study = Study.objects.exclude( archived = True ).filter(search__batch=batch).filter(dicom_uid=studyUid).filter( selected = True )[0]
        dicomDataFileList = [StudyDataFile( filename=filename, fileSize = filesize, study = study, archived = False ) for filename,filesize in zip( fileList, fileSizeList) ]
        qset = StudyDataFile.objects.filter(study=study)
        for dicomFile in dicomDataFileList:
            # if the file exists already, don't create a new file object unless needed
            if not overwrite:
                dicomFile.save()
            else:
                if not qset.count():
                    dicomFile.save()
                else:
                    dbFileSet = qset.filter(filename=dicomFile.filename)
                    if not dbFileSet:
                        dicomFile.save()
                    else:
                        dbFile = dbFileSet[0]
                        dbFile.fileSize = dicomFile.fileSize
                        dbFile.study = dicomFile.study
                        dbFile.save()

        # update the batch
        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.num_extractions_success = batch.num_extractions_success + 1
            batch.save(update_fields=['num_extractions_success'])
            study = Study.objects.select_for_update().filter( search__batch = batch ).filter(dicom_uid=studyUid).filter( selected = True )[0]
            study.status = Study.CO
            study.save()

    # If this is the last study in the group, then update the extraction datetime
    # if everything went well
    if ( batch.num_extractions_success + batch.num_extractions_failure ) == batch.num_extractions:

        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.extractions_running = False
            batch.save( update_fields=['extractions_running'])
            if batch.num_extractions_failure == 0:
                batch.extraction_datetime = timezone.now()
                batch.status = Batch.C
                batch.save(update_fields=['extraction_datetime','status'])
                batch.project.modification_date = batch.extraction_datetime.date()
                batch.project.save(update_fields=['modification_date'])

    return s


@shared_task(bind=True)
def extractSeries( self, seriesUid, studyUid, batchName, batchPK, projName ):
    seriesOptions = " -S -k 0020,000E=" + seriesUid + " " + '-k 0008,0052=SERIES' + ' -k 0020,000D='+studyUid
    args, workDir, stdoutFileName, stderrFileName = constructCommand( seriesOptions, studyUid, batchName, projName, seriesUid )
    stdoutFile = open( stdoutFileName, 'w')
    stderrFile = open( stderrFileName, 'w')

    s = 1
    try:
        s = subprocess.call( args,cwd=workDir, stdout=stdoutFile, stderr=stderrFile)
    except subprocess.CalledProcessError as e:
        print( str(e) )
    except Exception as e:
        print( 'Unknown error: {:s}'.format( str(e)))

    # if it returns did not work
    if s != 0:
        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.num_extractions_failure = batch.num_extractions_failure + 1
            batch.status = Batch.E
            # change the batch status if it is the last element of the batch and it is in error
            if (batch.num_extractions_failure + batch.num_extractions_success ) == batch.num_extractions:
                batch.extractions_running = False
            batch.save( update_fields=['num_extractions_failure','status','extractions_running'])
            series = Series.objects.select_for_update().exclude( archived = True ).filter( study__search__batch = batch ).filter( dicom_uid = seriesUid ).filter( selected = True )[0]
            series.status = Series.ER
            series.save()
            study = Study.objects.select_for_update().get(series=series)
            study.status = Study.ER
            study.save()
            return s

    # try and analyse the results
    rtrnStatus, overwrite, fileList, fileSizeList = analyseResults(studyUid, batchName, projName, seriesUid)

    # if the results were not good
    if not rtrnStatus:
        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.num_extractions_failure = batch.num_extractions_failure + 1
            batch.status = Batch.E
            # change the batch status if it is the last element of the batch and it is in error
            if (batch.num_extractions_failure + batch.num_extractions_success ) == batch.num_extractions:
                batch.extractions_running = False
            batch.save( update_fields=['num_extractions_failure','status','extractions_running'])
            series = Series.objects.select_for_update().exclude( archived = True ).filter( study__search__batch = batch ).filter( dicom_uid = seriesUid ).filter( selected = True )[0]
            series.status = Series.ER
            series.save()
            study = Study.objects.select_for_update().get(series=series)
            study.status = Study.ER
            study.save()
            return 1

    # create the list of datafiles, save them, and update the batch
    else:

        batch = Batch.objects.get(pk=batchPK)
        series = Series.objects.exclude(archived=True).filter(study__search__batch=batch).filter( dicom_uid=seriesUid).filter(selected=True)[0]
        dicomDataFileList = [SeriesDataFile(filename=filename, fileSize=filesize, series=series, archived = False ) for filename, filesize
                             in zip(fileList, fileSizeList)]
        qset = SeriesDataFile.objects.filter(series=series)
        for dicomFile in dicomDataFileList:
            # if the file exists already, don't create a new file object unless needed
            if not overwrite:
                dicomFile.save()
            else:
                if len( qset) == 0:
                    dicomFile.save()
                else:
                    dbFileSet = qset.filter(filename=dicomFile.filename)
                    if not dbFileSet:
                        dicomFile.save()
                    else:
                        dbFile = dbFileSet[0]
                        dbFile.fileSize = dicomFile.fileSize
                        dbFile.series = dicomFile.series
                        dbFile.save()

        # update the batch
        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.num_extractions_success = batch.num_extractions_success + 1
            batch.save(update_fields=['num_extractions_success'])
            series = Series.objects.select_for_update().exclude( archived = True ).filter( study__search__batch = batch ).filter( dicom_uid = seriesUid ).filter( selected = True )[0]
            series.status = Series.CO
            series.save()
            study = Study.objects.select_for_update().get(series=series)
            study.status = Study.CO
            study.save()


    # If this is the last series in the group, then update the extraction datetime
    # normally we arrive here if everything went well
    if ( batch.num_extractions_success + batch.num_extractions_failure ) == batch.num_extractions:

        with transaction.atomic():
            batch = Batch.objects.select_for_update().get(pk=batchPK)
            batch.extractions_running = False
            batch.save( update_fields=['extractions_running'])
            if batch.num_extractions_failure == 0:
                batch.extraction_datetime = timezone.now()
                batch.status = Batch.C
                batch.save(update_fields=['extraction_datetime','status'])
                batch.project.modification_date = batch.extraction_datetime.date()
                batch.project.save(update_fields=['modification_date'])

    return s

@shared_task( bind = True, max_retries=3 )
def testTask( self, delay, batchPK ):
    time.sleep(5)
    with transaction.atomic():
        batch = Batch.objects.select_for_update().get(pk=batchPK)
        batch.num_extractions_success = batch.num_extractions_success + 1
        batch.save( update_fields=['num_extractions_success'])
    return delay

@shared_task( bind = True )
def dummyTask( self, delay ):
    time.sleep(delay)
    return 0


def extractAllImages( batchPK ):

    batch = Batch.objects.get( pk = batchPK )
    batchName = slugify( batch.name )
    projName = slugify( batch.project.name )
    batch.status = Batch.S
    batch.extractions_running = True
    batch.save()

    seriesList = Series.objects.exclude(archived=True).filter(study__search__batch=batch).filter(selected=True)
    seriesUidList =  seriesList.values_list('dicom_uid', flat=True)
    seriesDirList =  seriesList.values_list('study__dicom_uid', flat=True)
    studiesList = Study.objects.exclude(archived=True).filter(search__batch=batch).filter(selected=True).exclude( series__study__search__batch = batch)
    studiesUidList =  studiesList.values_list('dicom_uid', flat=True).distinct()

    print( "#studies: {:d}  #series: {:d}".format( len(studiesList), len( seriesList ) ) )
    batch.num_extractions = len(studiesList) + len(seriesUidList)
    batch.num_extractions_success = 0
    batch.num_extractions_failure = 0
    batch.status = Batch.P
    batch.save()

    studiesGroup = [ extractStudy.s( studyUid, batchName, batchPK, projName ) for studyUid in studiesUidList ]
    seriesGroup = [ extractSeries.s( seriesUid, studyUid, batchName, batchPK, projName ) for seriesUid, studyUid in zip( seriesUidList, seriesDirList ) ]
    job = group( seriesGroup + studiesGroup )
    ret = job.apply_async( delay=2, queue='extract' )

    return ret


def retryExtraction( batchPK ):
    batch = Batch.objects.get( pk = batchPK )
    batchName = slugify( batch.name )
    projName = slugify( batch.project.name )

    seriesListAll = Series.objects.exclude(archived=True).filter(study__search__batch=batch).filter(selected=True)
    seriesListCompleted = seriesListAll.filter( status = Series.CO )
    seriesList = seriesListAll.exclude( status = Series.CO )
    seriesUidList =  seriesList.values_list('dicom_uid', flat=True)
    seriesDirList =  seriesList.values_list('study__dicom_uid', flat=True)
    studiesListAll = Study.objects.exclude(archived=True).filter(search__batch=batch).filter(selected=True).exclude( series__study__search__batch = batch)
    studiesListCompleted = studiesListAll.filter(status=Study.CO)
    studiesList = studiesListAll.exclude( status = Study.CO )
    studiesUidList =  studiesList.values_list('dicom_uid', flat=True).distinct()

    print("#studies: {:d}  #series: {:d}".format( len(studiesList), len( seriesList ) ) )
    batch.num_extractions = len(studiesListAll) + len(seriesListAll)
    batch.num_extractions_success = len( seriesListCompleted ) + len(studiesListCompleted)
    batch.num_extractions_failure = 0
    batch.status = Batch.P
    batch.extractions_running = True
    batch.save()

    studiesGroup = [ extractStudy.s( studyUid, batchName, batchPK, projName ) for studyUid in studiesUidList ]
    seriesGroup = [ extractSeries.s( seriesUid, studyUid, batchName, batchPK, projName ) for seriesUid, studyUid in zip( seriesUidList, seriesDirList ) ]
    job = group( seriesGroup + studiesGroup )
    ret = job.apply_async( delay=2, queue='extract' )
    return ret

@shared_task(bind=True,queue='search')
def runRemoteSearch( self, searchPK):
    search = Search.objects.get( pk = searchPK )
    searchTask = dicomSearch.runDicomStudySearch( search )
    return searchTask

@shared_task(bind=True)
def seriesSearch( self, studyPK ):
    study = Study.objects.get(pk=studyPK)
    study.seriesSearchStarted = True
    study.save( update_fields=["seriesSearchStarted"])
    seriesCount = dicomSearch.runDicomSeriesSearch( studyPK )
    study.numSeriesResults = seriesCount
    study.seriesSearchFinished = True
    study.save(update_fields=["numSeriesResults","seriesSearchFinished"])

def runRemoteSeriesSearch( searchPK ):
    search = Search.objects.get( pk = searchPK )
    studyList = Study.objects.filter( search = search ).filter( doSeriesSearch = True )
    if len(studyList) > 0:
        seriesGroup = [ seriesSearch.s( study.pk ) for study in studyList ]
        job = group( seriesGroup )
        ret = job.apply_async( delay = 2, queue='search' )
        return ret
    else:
        return 0



