# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2018-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

import subprocess
import threading

from datetime import datetime
from time import sleep
from pathlib import Path
from lxml import etree
import os

def backup_file( file_path, file_name ):
    '''
    Backup a file by appending the date and time to the file name.

    :param file_path: the directory where the file to be backed up is
    :param file_name: the name of the file
    :return: None
    '''
    orig_file = Path(file_path).joinpath(file_name)
    update_file = Path(file_path).joinpath(file_name + '_{:s}'.format(datetime.now().strftime('%Y%m%dT%H%M%S')))
    orig_file.rename(update_file)

def run_cmd( cmd ):
    '''
    Run a command in the OS.
    :param cmd:
    :return: None or a CalledProcessError Exception
    '''
    args = cmd.split(' ')
    try:
        rtrn = subprocess.run(args, stdout=subprocess.PIPE, stdin=None, stderr=subprocess.PIPE, shell=False, universal_newlines=False)
    except subprocess.CalledProcessError as e:
        print('Called process error: {:s}'.format(str(e)))
        return rtrn
    return rtrn

class ctp_utils:
    ''''
    The class takes care of setting up the interface to the Clinical Trials Processor (CTP). It includes commands to set up the anonymisation pipeline in CTP as well as the samba share point.
    '''

    def __init__(self, ctp_base_dir = None, ctp_data_dir=None, pacs_data_dir=None, ctp_daemon='ctpd', samba_daemon='smbd', samba_config_file='/etc/samba/smb.conf' ):
        '''

        :param ctp_base_dir: the directory where CTP is installed
        :param ctp_data_dir: the directory where CTP puts the anonymized data to be distributed in the SAMBA share
        :param pacs_data_dir: the directory where Radiomics Enabler stores the extracted data files
        :param ctp_daemon:  the name of the CTP daemon to be used with the sudo service restart command
        :param samba_daemon: the name of the samba daemon to be used with the sudo serivce restart command
        :param samba_config_file: the name of the samba configuration file
        '''

        # check that the values were set
        if ( ctp_base_dir is None ) or ( ctp_data_dir is None ) or ( pacs_data_dir is None ):
            raise ValueError( 'The data and configuration directories must be defined not None: ctp_base_dir={:s}  ctp_data_dir={:s} pacs_data_dir={:s}'.format( ctp_base_dir, ctp_data_dir,pacs_data_dir)  )

        # check that the input variables are valid directories
        if not Path( ctp_base_dir ).is_dir() or not Path( ctp_data_dir ).is_dir() or not Path( pacs_data_dir ).is_dir():
            raise ValueError( 'The data and configuration directories must be valid directories: ctp_base_dir={:s}  ctp_data_dir={:s} pacs_data_dir={:s}'.format( ctp_base_dir, ctp_data_dir,pacs_data_dir)  )

        # set the values for the data
        self._ctp_base_dir = ctp_base_dir
        self._ctp_data_dir = ctp_data_dir
        self._pacs_data_dir = pacs_data_dir

        # the daemon names are used to restart the daemons, if they are None, the daemons are not restarted
        self._ctp_daemon = ctp_daemon
        self._samba_daemon = samba_daemon

        # samba config file
        self._smb_cfg_file = samba_config_file

        # the number of seconds (roughly) to wait for an update of the CTP configuration script
        self._n_secs_wait = 20



    def add_project(self, new_project_name ):
        ''' Add a new project to CTP.
         The CTP system is updated using he following steps:
            1) creating the pipeline configuration,
            2) restarting CTP,
            3) updating the anonymizer script,
            4) restarting CTP,
            5) updating the samba configuration
            6) restarting samba
        :param new_project_name:
        :return: None
        '''
        sleep(10)
        self.stop_ctp()
        tree = self.add_pipeline( new_project_name )
        self.backup_config()
        self.save_config(tree)
        self.start_ctp()
        self.update_anonymizer_script(new_project_name)
        self.restart_ctp()
        self.update_samba_config(new_project_name,self._smb_cfg_file)
        self.restart_samba()


    def add_project_as_thread(self, new_project_name ):
        thread = threading.Thread( target = self.add_project, args = (new_project_name,) )
        thread.daemon = True
        thread.start()

    def stop_ctp(self):
        if not self._ctp_daemon is None:
            status = run_cmd('sudo service {:s} status'.format(self._ctp_daemon))
            if status.returncode == 0:
                status = run_cmd('sudo service {:s} stop'.format(self._ctp_daemon))
            elif status.returncode != 0 and 'not-found' in status.stdout.decode():
                print('Could not stop {:s} status message was {:s}'.format(self._ctp_daemon, status.stdout.decode()))
            elif status.returncode != 0 and not 'not-found' in status.stdout.decode():
                print('{:s} is already stopped'.format(self._ctp_daemon))
            sleep(2)

    def start_ctp(self):
        if not self._ctp_daemon is None:
            status = run_cmd('sudo service {:s} status'.format(self._ctp_daemon))
            if status.returncode != 0 and not 'not-found' in status.stdout.decode():
                status = run_cmd( 'sudo service {:s} start'.format( self._ctp_daemon ) )
            elif status.returncode != 0 and 'not-found' in status.stdout.decode():
                print('Could not start {:s} status message was {:s}'.format( self._ctp_daemon, status.stdout.decode() ) )
            elif status.returncode == 0:
                print( '{:s} is already running'.format(self._ctp_daemon) )
            sleep(2)

    def restart_ctp(self):
        '''
        Restart CTP to update working directories etc.
        :return:  None
        '''
        self.stop_ctp()
        self.start_ctp()

    def update_samba_config(self, new_project_name, smb_cfg_file='/etc/samba/smb.conf' ):
        '''Update the Samba configuration so that the anonymized output is available for users.'''
        with open( smb_cfg_file, 'a' ) as smbconf:
            smbconf.write( '\n[{:s}]\n'.format(new_project_name) )
            smbconf.write('path = {:s}\n'.format(os.path.join( self._ctp_data_dir,new_project_name)) )
            smbconf.write('browsable = yes\nguest ok = yes\nread only = no\ncreate mask = 0444')

    def restart_samba(self):
        '''Restart the samba daemon'''
        if not self._samba_daemon is None:
            status = run_cmd('sudo service {:s} status'.format(self._samba_daemon))
            if status.returncode == 0:
                status = run_cmd( 'sudo service {:s} stop'.format( self._samba_daemon ) )
                status = run_cmd('sudo service {:s} start'.format(self._samba_daemon))
            elif status.returncode != 0 and not 'not-found' in status.stdout.decode():
                status = run_cmd('sudo service {:s} start'.format(self._samba_daemon))
            if status.returncode != 0 and 'not-found' in status.stdout.decode():
                print('Could not start {:s} status message was {:s}'.format(self._samba_daemon, status.stdout.decode()))

    def get_config_tree(self, fname ='config.xml'):
        '''
        Load the CTP configuration file
        :param fname: the name of the CTP configuration file
        :return: the XML tree data structure from the etree.parse command.
        '''
        configFile = Path( self._ctp_base_dir ).joinpath( fname )
        if not configFile.exists():
            raise ValueError( 'Cannot parse config file: {:s} does not exist'.format( str( configFile ) ) )
        parser = etree.XMLParser(remove_blank_text=True)
        tree = etree.parse( os.path.join( self._ctp_base_dir, fname ), parser )
        return tree

    def backup_config(self,fname='config.xml'):
        '''
        Backup the CTP configuation file.
        :param fname: the name of the CTP configuration file
        :return:
        '''
        backup_file(self._ctp_base_dir, fname)

    def save_config(self, tree, fname='config.xml'):
        '''
        Write the CTP configuration file.
        :param tree: An etree structure corresponding to the XML format CTP configuration file.
        :param fname: the name of the configuration file
        :return: None
        '''
        tree.write( os.path.join(self._ctp_base_dir, fname ), pretty_print=True )

    def add_pipeline( self, new_project_name, tree = None ):
        '''
        Add a pipeline corresponding to a new project to CTP
        :param new_project_name: The name of the new project
        :param tree: the etree data structure ofthe configuration file to be modified.
        :return: None
        '''
        if tree is None:
            tree = self.get_config_tree()
        root = tree.getroot()

        # pipeline element
        pipeline = etree.SubElement(root,'Pipeline')
        pipeline.set('name', new_project_name)
        pipeline.set('root', str( os.path.join( self._ctp_data_dir , new_project_name)  ) )

        # directory import service
        directoryimportservice = etree.SubElement(pipeline, 'DirectoryImportService')
        directoryimportservice.set('class', 'org.rsna.ctp.stdstages.DirectoryImportService')
        directoryimportservice.set('filePathTag', '[0010,4000]')
        directoryimportservice.set('import',os.path.join(self._pacs_data_dir ,new_project_name) )
        directoryimportservice.set('name', 'DirectoryImportService')
        directoryimportservice.set('quarantine', os.path.join('quarantines', new_project_name,'DirectoryImportService') )
        directoryimportservice.set('root',os.path.join('roots' ,'DirectoryImportService') )

        # id map
        idmap = etree.SubElement(pipeline, 'IDMap')
        idmap.set('class', 'org.rsna.ctp.stdstages.IDMap')
        idmap.set('name', 'IDMap')
        idmap.set('root', 'roots/IDMap')

        # anonymizer
        dicomanonymizer = etree.SubElement(pipeline, 'DicomAnonymizer')
        dicomanonymizer.set('class', 'org.rsna.ctp.stdstages.DicomAnonymizer')
        dicomanonymizer.set('lookupTable', os.path.join('scripts', new_project_name,'LookupTable.properties') )
        dicomanonymizer.set('name', 'DicomAnonymizer')
        dicomanonymizer.set('quarantine', os.path.join('quarantines', new_project_name ,'DicomAnonymizer') )
        dicomanonymizer.set('script', os.path.join( 'scripts' , new_project_name, 'DicomAnonymizer.script') )

        # directory storage
        directorystorageservice = etree.SubElement(pipeline, 'DirectoryStorageService')
        directorystorageservice.set('acceptDuplicates','no')
        directorystorageservice.set('acceptFileObjects','no')
        directorystorageservice.set('acceptXmlObjects','no')
        directorystorageservice.set('acceptZipObjects','no')
        directorystorageservice.set('class','org.rsna.ctp.stdstages.DirectoryStorageService')
        directorystorageservice.set('defaultString','')
        directorystorageservice.set('logDuplicates','no')
        directorystorageservice.set('name','DirectoryStorageService')
        directorystorageservice.set('quarantine',os.path.join('quarantines', new_project_name ,'DirectoryStorageService') )
        directorystorageservice.set('root', os.path.join( self._ctp_data_dir , new_project_name) )
        directorystorageservice.set('setStandardExtensions','yes')
        directorystorageservice.set('structure','[0010,4000]/[0010,0020]/[0008,0020]/[0008,103e]')
        directorystorageservice.set('whitespaceReplacement','_')

        return tree

    def update_anonymizer_script(self, new_project_name):
        '''
        Update the anonymizer script created by CTP by default to add the batch name to a part of the Dicom Info.
        :param new_project_name: the name of the new project.
        :return: None.
        '''

        script_file = os.path.join( self._ctp_base_dir,'scripts',new_project_name,'DicomAnonymizer.script')

        # There is a delay in the creation of the new files after restarting the CTP daemon process, so we need to
        # wait a little while,
        cnt = 0
        while cnt < self._n_secs_wait and not os.path.exists( script_file ) :
            sleep(1)
            cnt = cnt + 1

        # Throw an error if we still don't find the file, otherwise edit it
        if not os.path.exists( script_file ) :
            raise ValueError( 'Validation script does not exist, {:s} '.format( script_file) )

        tree = etree.parse( script_file )
        root=tree.getroot()
        tag = '00104000'
        script = "@contents(this,\"(^[^/]*/)|(/[^/]*$)\")"
        for e in root.findall('e'):
            if e.get('t') == tag:
                e.text = script
                break
        backup_file(  os.path.join( self._ctp_base_dir,'scripts',new_project_name ), 'DicomAnonymizer.script' )
        tree.write( script_file, pretty_print=True )

    def print_pipeline_names(self):
        '''
        Utility to print the names of the existing pipelines in the CTP configuration.
        :return: None
        '''
        tree = self.get_config_tree()
        for Pipeline in tree.xpath('/Configuration/Pipeline'):
            print(Pipeline.get('name'))

    def print_import_dir(self):
        '''
        Utility to the print the import directories associated with each CTP pipeline.
        :return:
        '''
        tree = self.get_config_tree()
        for Pipeline in tree.findall('Pipeline'):
            name = Pipeline.get('name')
            for DirectoryImportService in Pipeline.iter('DirectoryImportService'):
                importDir = DirectoryImportService.attrib.get('import')
                print( name + ' : ' + importDir )


# testing files.
if __name__ == '__main__':

    ctp = ctp_utils( '/mnt/bigdisk/data16/mike16/ctp/CTP'  , '/mnt/bigdisk/data16/mike16/ctp/ctp_data', '/mnt/bigdisk/data16/mike16/radenab/data/RadEnab14/data' )


    #
    ctp.print_pipeline_names()
    ctp.print_import_dir()
    ctp.add_project( 'normal_project4')
    ctp.add_project_as_thread( 'thread_project4')
    sleep(30)
    ctp.print_pipeline_names()
    ctp.print_import_dir()

    # # samba config
    # ctp.update_samba_config( 'test_project')
    #
    #
    # # no daemon restart
    # ctp._ctp_daemon = None
    # ctp.restart_ctp()
    #
    # # ctp daemon restart w & wo error
    # try:
    #     print('testing ctp naming error')
    #     ctp._ctp_daemon = 'ctpe'
    #     ctp.restart_ctp()
    # except:
    #     print('ctpe: caught error')
    #
    # try:
    #     print('testing ctpd')
    #     ctp._ctp_daemon = 'ctpd'
    #     ctp.restart_ctp()
    #     print( 'ctpd worked')
    # except:
    #     print('ctpd: caught error')
    #
    # # no daemon restart
    # ctp._samba_daemon = None
    # ctp.restart_samba()
    #
    # # daemon restart w & wo error
    # try:
    #     print('\ntesting samba naming error')
    #     ctp._samba_daemon = 'smbe'
    #     ctp.restart_samba()
    # except:
    #     print('smbe: caught error')
    # try:
    #     print('testing smbd')
    #     ctp._samba_daemon = 'smbd'
    #     ctp.restart_samba()
    #     print( 'smbd worked')
    # except:
    #     print('smbd: caught error')


    #ctp.add_pipeline( 'testPipeline')
    #ctp.restart_ctp()
    #ctp.add_project('testProject')
    #ctp.update_anonymizer_script( 'testProject')