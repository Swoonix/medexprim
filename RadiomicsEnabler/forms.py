# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

from django import forms
from .models import Project, Batch, Search
from django.forms.widgets import SelectMultiple
import datetime


class ProjectForm(forms.ModelForm):

    class Meta:
        model = Project
        exclude = ['modification_date','creation_date','destination','owner']


class BatchForm(forms.ModelForm):

    class Meta:
        model = Batch
        fields = [ 'name', 'description' ]

class QueryForm( forms.ModelForm ):

    class Meta:
        model = Search
        exclude = [ 'batch','archived', 'advanced_search' ]
        fields = ['study_date_from','study_date_to','patient_last_name','patient_first_name','patient_birth_date','patient_id','patient_list','modality','exam_types' ]
        now = datetime.datetime.now()
        yearRange = range( now.year, 1900-1,-1)
        widgets = { #'study_date_from': AdminDateWidget(),  # AdminDateWidget(), # SelectDateWidget(years=yearRange),
                    #'study_date_to':AdminDateWidget(), #AdminDateWidget(), #SelectDateWidget(years=yearRange),
                    #'patient_birth_date':AdminDateWidget(), #AdminDateWidget(), #SelectDateWidget(years=yearRange),
                    'modality':SelectMultiple(choices=Search.modality_choices)
        }


    # the user needs to put in at least one query element
    def clean(self):
        if not self.has_changed():
            raise forms.ValidationError( ("You must fill at least one field!"), "emptyForm")
        cleaned_data = super(QueryForm, self).clean()
        return cleaned_data

class ListQueryForm( forms.ModelForm ):

    class Meta:
        model = Search
        exclude = [ 'batch','archived', 'advanced_search','patient_last_name','patient_first_name','patient_birth_date','patient_id','patient_list' ]
        fields = ['study_date_from','study_date_to','modality','exam_types' ]
        now = datetime.datetime.now()
        yearRange = range( now.year, 1900-1,-1)
        widgets = { 'modality':SelectMultiple(choices=Search.modality_choices) }

# this is a dummy form that is used to have access to Form error messages in the Study search and Series search results diaglog pages.
class SearchForm( forms.Form ):
    dummy = forms.CharField(widget=forms.HiddenInput(),required=False)


class BatchExtractForm( forms.Form ):
    hoursChoices = ['%s:%s%s' % (h, m, ap) for ap in ('am', 'pm') for h in ([12] + list(range(1,12))) for m in ('00', '30')]
    limitExtraction = forms.BooleanField( label="Limit extraction operations ", initial=False)
    beginHour = forms.ChoiceField( choices=hoursChoices, label='from', initial="9:00pm")
    endHour = forms.ChoiceField(choices=hoursChoices, label = 'to', initial="4:00am")

