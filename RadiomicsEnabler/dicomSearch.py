# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

import pydicom, glob
import os.path
import tempfile, shlex, subprocess
from datetime import date
from sitebase import settings
from .models import Study, Series, Search, Batch
from django.forms import ValidationError
import sys
from datetime import datetime
from django.conf import settings
import tokenize
import time
from subprocess import check_output

def toDicomDate( dateValue):
    return '{:d}{:02d}{:02d}'.format( dateValue.year, dateValue.month, dateValue.day )


def formatDicomKey( key, value):
    return ' -k "' + key + '=' + value + '"'


def idKey( search):
    idKeyCode = '(0010,0020)'
    value = search.patient_id
    return formatDicomKey(idKeyCode, value)


def nameKey( search):
    nameKeyCode = '(0010,0010)'
    value = ''
    if search.patient_last_name:
        value = search.patient_last_name
    if search.patient_first_name:
        value = value + '^' + search.patient_first_name
    return formatDicomKey(nameKeyCode, value)


def birthDateKey( search):
    bdKeyCode = '(0010,0030)'
    value = ''
    if search.patient_birth_date:
        value = toDicomDate(search.patient_birth_date)
        value = value + '-' + value
    return formatDicomKey(bdKeyCode, value)


def studyDateKey( search):
    studyDateCode = '(0008,0020)'
    value = ''
    if search.study_date_from and search.study_date_to:
        value = toDicomDate(search.study_date_from) + '-' + toDicomDate(search.study_date_to)
    elif search.study_date_from and (search.study_date_to == ''):
        todayValue = date.today()
        value = toDicomDate(search.study_date_from) + '-' + '{:d}{:0d}{:0d}'.format(todayValue.year,
                                                                                                todayValue.month,
                                                                                                todayValue.day)
    elif search.study_date_to and (search.study_date_from == ''):
        value = '19000101-' + toDicomDate(search.study_date_to)
    return formatDicomKey(studyDateCode, value)

def modalitiesInStudy( search ):
    modalitiesInStudyCode = '(0008,0061)'
    value=''
    if search.modality:
        value = search.modality
    return formatDicomKey( modalitiesInStudyCode, value )

def iterated_search( thisSrchStr, tmpDir ):

    with open(os.path.join(tmpDir, 'findscu.txt'), 'w', encoding='utf-8' ) as text_file:
        text_file.write( thisSrchStr )
        text_file.flush()
        text_file.close()

    args = shlex.split(thisSrchStr)

    stdoutFile = open(os.path.join(tmpDir, 'stdout.log'), 'w')
    stderrFile = open(os.path.join(tmpDir, 'stderr.log'), 'w')

    # add a loop to retry a specific number of times
    maxTrials = getattr(settings, 'RAD_ENAB_MAX_RETRIES',  3 )
    retryDelay = getattr(settings, 'RAD_ENAB_RETRY_DELAY', 1 )
    trialCnt = 0
    dcmFileList = list()
    endTrials = False
    while not endTrials:
        s = subprocess.call(args, cwd=tmpDir, stdout=stdoutFile, stderr=stderrFile)

        # we can end the loop if we actually got a response from the PACS
        dcmFileList = glob.glob(os.path.join(tmpDir, "*.dcm"))
        if len( dcmFileList ) > 0:
            endTrials = True

        # number of trials
        trialCnt = trialCnt + 1
        if trialCnt == maxTrials:
            endTrials = True

        if not endTrials:
            stderrFile.write( "Iteration {:d}\n".format( trialCnt ) )
            time.sleep( retryDelay )

    return dcmFileList


def analyse_study_dicom_file_list(search, dcmFileList):

    # evaluate the study values
    batch = Batch.objects.get( search = search )
    existingStudyUIDList = [ u['dicom_uid'] for u in Study.objects.filter(search__batch=batch).filter(selected=True).values('dicom_uid')]

    rtrnCnt = 0
    for dcmfile in dcmFileList:
        studyData = pydicom.dcmread(dcmfile)
        doSave = True
        # don't add existing already selected studies to the results list.
        if studyData.StudyInstanceUID in str(existingStudyUIDList):
            doSave = False
            print('Skipping {:s} -- already selected'.format(  studyData.StudyInstanceUID ))
            continue
        try:
            studyDataModalities = ''
            if 'ModalitiesInStudy' in studyData:
                if isinstance(studyData.ModalitiesInStudy, str ):
                    studyDataModalities = studyData.ModalitiesInStudy
                else:
                    studyDataModalities = ','.join( studyData.ModalitiesInStudy )
            study_id = ''
            if 'StudyID' in studyData:
                study_id = studyData.StudyID
            patient_id = ''
            if 'PatientID' in studyData:
                patient_id = studyData.PatientID
            study = Study(patient_id=patient_id,
                          modality=studyDataModalities,
                          study_id=study_id,
                          dicom_uid=studyData.StudyInstanceUID,
                          patient_last_name=studyData.PatientName.family_name,
                          patient_first_name=studyData.PatientName.given_name,
                          search=search)

            if 'StudyDate' in studyData:
                if len( studyData.StudyDate ) >= 8:
                    study_date = studyData.StudyDate[0:4] + '-' + studyData.StudyDate[4:6] + '-' + studyData.StudyDate[6:8]
                    study.study_date = study_date

            if 'PatientBirthDate' in studyData:
                if len( studyData.PatientBirthDate ) >= 8:
                    patient_birth_date = studyData.PatientBirthDate[0:4] + '-' + studyData.PatientBirthDate[4:6] + '-' + studyData.PatientBirthDate[6:8]
                    study.patient_birth_date = patient_birth_date

            # The choice of what to put in the study description field of the database
            rpd = ''
            if 'RequestedProcedureDescription' in studyData:
                 rpd = studyData.RequestedProcedureDescription
            sd = ''
            if 'StudyDescription' in studyData:
                sd = studyData.StudyDescription
            if  getattr(settings, 'RAD_ENAB_STUDY_DESCRIPTION_SOURCE', 'StudyDescription') == 'StudyDescription':
                study.study_description = sd
            else:
                study.study_description = rpd

            if doSave and search.modality and not search.exam_types:
                doSave = False
                # if we receive a search modality as a string list ...
                if '[' in search.modality:
                    modalityWrk = eval(search.modality, {'__builtins__': None}, {})
                    for modality in modalityWrk:
                        if modality.upper() in study.modality.upper():
                            doSave = True
                            break
                else:
                    if search.modality.upper() in study.modality.upper():
                        doSave = True
            if doSave and not search.modality and search.exam_types:
                doSave = False
                for examType in search.exam_types.upper().split(','):
                    if examType in study.study_description.upper():
                        doSave = True
                        break
            if doSave and search.modality and search.exam_types:
                doSave = False
                if search.exam_types.upper() in study.study_description.upper():
                    # if we receive a search modality as a string list ...
                    if '[' in search.modality:
                        modalityWrk = eval(search.modality, {'__builtins__': None}, {})
                        for modality in modalityWrk:
                            if modality.upper() in study.modality.upper():
                                doSave = True
                                break
                    else:
                        if search.modality.upper() in study.modality.upper():
                            doSave = True

            if doSave:
                study.save()
                rtrnCnt = rtrnCnt + 1

        except AttributeError as err:
            print( str( err ) + " -- attribute error for " + dcmfile)
        except ValidationError as err:
            print( str( err ) +  " -- validation error for " + dcmfile)
        except:
            print( "Unexpected error:" +  str( sys.exc_info()[0] ) )
            print(  "unknown error for " + dcmfile )
    return rtrnCnt



def doStudySearch(search):
    medexprimAetID = getattr(settings, 'PACS_AE_TITLE', 'RADENAB')
    medexprimDicomServer = getattr(settings, 'PACS_DICOM_SERVER', 'www.dicomserver.co.uk')
    medexprimDicomPort = getattr(settings, 'PACS_FIND_SCP_PORT','11112')
    medexprimAEtitle = getattr(settings, 'RAD_ENAB_AE_TITLE', 'RADENAB_QUERY_SCU')
    medexprimWorkDir  = getattr(settings,'RAD_ENAB_WORK_DIR','/data/work' )
    findscu_cmd = getattr(settings, 'RAD_ENAB_FIND_SCU', '/usr/bin/findscu' )

    basestr = findscu_cmd + ' -X -S --aetitle ' + medexprimAEtitle + ' --call ' + medexprimAetID + ' -k "(0008,0052)=STUDY"'
    srchStr = basestr \
              + nameKey(search) \
              + idKey(search) \
              + birthDateKey(search) \
              + studyDateKey(search) \
              + modalitiesInStudy( search ) \
              + formatDicomKey('(0020,0010)', '') \
              + formatDicomKey('(0020,000D)', '') \
              + formatDicomKey('(0008,0080)', '') \
              + formatDicomKey('(0032,1060)', '') \
              + formatDicomKey('(0008,1010)', '') \
              + formatDicomKey('(0008,1030)', '')

    thisSrchStr = srchStr + ' ' + medexprimDicomServer + ' ' + medexprimDicomPort

    tmpDir = tempfile.mkdtemp('', 'tmpStudy', medexprimWorkDir)

    dcmFileList = iterated_search( thisSrchStr, tmpDir )

    rtrnCnt = analyse_study_dicom_file_list(search, dcmFileList)

    search.numberOfResults = search.numberOfResults + rtrnCnt
    search.notRun = False
    search.save()
    return rtrnCnt

def try_parsing_date(text):
    for fmt in ('%d/%m/%Y', '%Y/%m/%d','%Y-%m-%d','%d.%m.%Y', '%d/%m/%Y'):
        try:
            return datetime.strptime(text, fmt).date()
        except ValueError:
            pass
    return None

# We save the search.modality as a string, need to extract the values to make a list
# to iterate over
def modalityList( search ):
    rtrn = list()
    modalityKeys = dict( Search.modality_choices ).keys()
    for tmp in search.modality.split("'"):
        if tmp in modalityKeys:
            rtrn.append( tmp )
    return rtrn

def runDicomStudySearch( search):

    # we iterate over the different modalities for each query
    # the querys are saved as string representation of a list, which is inconvenient
    # this
    search_modality_list = None
    if search.modality:
        search_modality_list = modalityList( search )


    if search.patient_list:
        rowCnt = 0
        studyCnt = 0
        errCnt = 0
        rowError = []
        patientListFile = os.path.join(settings.MEDIA_ROOT, search.patient_list.name)

        with tokenize.open(patientListFile) as csvfile:
            firstLine = csvfile.readline()
            csvfile.seek(0)
            stringSep = ','
            if stringSep not in firstLine:
                stringSep = ';'
            if stringSep not in firstLine:
                stringSep = None
            if not stringSep is None:
                for line in csvfile:
                    row = line.strip().split( stringSep )
                    rowCnt = rowCnt + 1
                    if len(row) >= 3 and len( row[2] ) > 0:
                        search.patient_last_name = row[0]
                        search.patient_first_name = row[1]
                        search.patient_birth_date = try_parsing_date( row[2] )
                        if search_modality_list:
                            #modalityWrk = eval(search.modality, {'__builtins__': None}, {})
                            for modality in search_modality_list:
                                search.modality = modality
                                tmpCnt = doStudySearch(search)
                                if tmpCnt > 0:
                                    studyCnt = studyCnt + tmpCnt
                                else:
                                    errCnt = errCnt + 1
                                    rowError.append( row[0]+',' + row[1]+',' + row[2] + ',modality:' + modality )
                        else:
                            tmpCnt = doStudySearch(search)
                            if tmpCnt > 0:
                                studyCnt = studyCnt + tmpCnt
                            else:
                                errCnt = errCnt + 1
                                rowError.append(row[0] + ',' + row[1] + ',' + row[2] )

                    else:
                        errCnt = errCnt + 1
                        rowError.append( ','.join( row ) )

            else:
                for row in csvfile:
                    if row.strip():
                        rowCnt = rowCnt + 1
                        search.patient_id = row.strip()
                        if search_modality_list:
                            #modalityWrk = eval(search.modality, {'__builtins__': None}, {})
                            for modality in search_modality_list:
                                search.modality = modality
                                tmpCnt = doStudySearch(search)
                                if tmpCnt > 0:
                                    studyCnt = studyCnt + tmpCnt
                                else:
                                    errCnt = errCnt + 1
                                    rowError.append(row + ',modality:' + modality )
                        else:
                            tmpCnt = doStudySearch(search)
                            if tmpCnt > 0:
                                studyCnt = studyCnt + tmpCnt
                            else:
                                errCnt = errCnt + 1
                                rowError.append(row )


            rtrnStatus = "Found {:d} matching study(ies) for {:d} input row(s)".format(  studyCnt, rowCnt )
            if errCnt > 0:
                rtrnStatus = rtrnStatus + " and {:d} error(s)".format( errCnt )
                rtrnStatus = rtrnStatus + " for file:" + search.patient_list.name + '.'
                rtrnStatus = rtrnStatus + 'File lines with errors are:' + ';'.join( rowError )
            rtrnStatus = rtrnStatus + '.'


    else:
        # if the PACS supports modality in study, then we search for each modality separately
        if search_modality_list:
            numStudies = 0
            #modalityWrk = eval(search.modality, {'__builtins__': None}, {})
            for modality in search_modality_list:
                search.modality = modality
                numStudies = numStudies + doStudySearch(search)
            rtrnStatus = "Found {:d} matching studies".format(numStudies)
        else:
            numStudies = doStudySearch(search)
            rtrnStatus = "Found {:d} matching studies".format(  numStudies )

    # finalise the status
    search.rtrnStatus = rtrnStatus
    search.isFinished = True
    search.save()

    return rtrnStatus

def runDicomSeriesSearch( studyPK):
    # /usr/bin/findscu -X -S --aetitle PROL_QUERY_SCU --call  REmedex  -k "(0008,0052)=SERIES" -k "(0020,000d)=1.2.826.0.1.3680043.6.2416.6218.20161109124536.336.2"
    # -k "(0020,000e)=" -k "(0010,0010)=" -k "(0010,0020)=" -k "(0010,0030)=" -k "(0008,0020)=" -k "(0032,1060)=" -k "(0008,0060)=" -k "(0008,103e)=" www.dicomserver.co.uk 11112 >& tmp.log

    study = Study.objects.get(pk=studyPK)

    medexprimAetID = getattr(settings, 'PACS_AE_TITLE', 'RADENAB')
    medexprimDicomServer = getattr(settings, 'PACS_DICOM_SERVER', 'www.dicomserver.co.uk')
    medexprimDicomPort = getattr(settings, 'PACS_FIND_SCP_PORT','11112')
    medexprimAEtitle = getattr(settings, 'RAD_ENAB_AE_TITLE', 'RADENAB_QUERY_SCU')
    medexprimWorkDir  = getattr(settings,'RAD_ENAB_WORK_DIR','/data/work' )
    findscu_cmd = getattr(settings, 'RAD_ENAB_FIND_SCU', '/usr/bin/findscu' )

    basestr = findscu_cmd +' -X -S --aetitle ' + medexprimAEtitle + ' --call ' + medexprimAetID + ' -k "(0008,0052)=SERIES"'
    srchStr = basestr \
              + formatDicomKey('(0020,000D)', study.dicom_uid) \
              + formatDicomKey('(0020,000e)', '') \
              + formatDicomKey('(0010,0010)', '') \
              + formatDicomKey('(0010,0020)', '') \
              + formatDicomKey('(0010,0030)', '') \
              + formatDicomKey('(0008,0020)', '') \
              + formatDicomKey('(0032,1060)', '') \
              + formatDicomKey('(0008,0060)', '') \
              + formatDicomKey('(0008,103E)', '') \
              + formatDicomKey('(0032,1060)', '')

    thisSrchStr = srchStr + ' ' + medexprimDicomServer + ' ' + medexprimDicomPort

    tmpDir = tempfile.mkdtemp('', 'tmpSeries', medexprimWorkDir)

    dcmFileList = iterated_search( thisSrchStr, tmpDir )

    # we don't want to add series that are already associated to the studies
    seriesCnt = 0
    currentSeries = list(Series.objects.filter(study=study).filter(selected=True).values_list('dicom_uid', flat=True))
    for dcmfile in dcmFileList:
        seriesData = pydicom.dcmread(dcmfile)
        if not "{:s}".format(seriesData.SeriesInstanceUID) in currentSeries:
            seriesCnt = seriesCnt + 1
            seriesFields = seriesData.dir()
            series = Series(study=study,
                            dicom_uid=seriesData.SeriesInstanceUID,
                            selected=False,
                            archived=False)
            if 'SeriesDescription' in seriesFields:
                series.series_description = seriesData.SeriesDescription
            if 'Modality' in seriesFields:
                series.modality = seriesData.Modality
            print( series.dicom_uid )
            series.save()
        else:
            print('Skipping series {:s} as it is already selected'.format( seriesData.SeriesInstanceUID ))
    return seriesCnt
