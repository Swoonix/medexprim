# Radiomics Enabler(R) - Unleash the potential of your medical images archives (PACS) for research
#
#     Copyright (C) 2017-2019 Medexprim
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as
#     published by the Free Software Foundation, either version 3 of the
#     License, or (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see https://www.gnu.org/licenses/.
#
#     Contact : contact@medexprim.com

import django_tables2 as tables
from django_tables2.utils import A
from .models import Project, Batch
from django.utils.safestring import mark_safe
from sitebase.settings import DATETIME_FORMAT
from django.contrib.staticfiles.templatetags.staticfiles import static


def formatDatetime( value ):
    if not value is None:
        return value.strftime( DATETIME_FORMAT)
    else:
        return ''

class ProjectTable(tables.Table):
    name = tables.LinkColumn('projectDetails',args=[A('pk')])
    modify = tables.LinkColumn('projectModify',args=[A('pk')], text=mark_safe("<img src=\"{:s}\" alt=\"Modify\"/>".format( static( 'RadiomicsEnabler/images/icon-changelink.svg' ))), orderable=False, verbose_name = "Modify" )
    delete = tables.LinkColumn('projectArchive',args=[A('pk')], text=mark_safe("<img src=\"{:s}\" alt=\"Archive\"/>".format(static('RadiomicsEnabler/images/icon-deletelink.svg'))), orderable=False, verbose_name = "Delete" )


    class Meta:
        model = Project
        sequence = ('name','owner','creation_date','modification_date')
        fields = ('name','owner','creation_date','modification_date')
        order_by = '-creation_date'

        # add class="paleblue" to <table> tag
        attrs = {'class': 'paleblue'}


class BatchWaitingTable(tables.Table):
    name = tables.LinkColumn('batchDetail',args=[A('pk')])
    modify = tables.LinkColumn('batchModify',args=[A('pk')], text=mark_safe("<img src=\"{:s}\" alt=\"Modify\"/>".format(static('RadiomicsEnabler/images/icon-changelink.svg'))), orderable=False, verbose_name = "Modify" )
    delete = tables.LinkColumn('batchArchive',args=[A('pk')], text=mark_safe("<img src=\"{:s}\" alt=\"Archive\"/>".format(static('RadiomicsEnabler/images/icon-deletelink.svg'))), orderable=False, verbose_name = "Delete" )

    class Meta:
        model = Batch
        sequence = ( 'name', 'preparation_datetime', 'number_of_patients', 'number_of_studies', 'number_of_series' )
        fields = ( 'name', 'preparation_datetime', 'number_of_patients', 'number_of_studies', 'number_of_series' )
        order_by = '-preparation_datetime'

        # add class="paleblue" to <table> tag
        attrs = {'class': 'paleblue', 'id':'tablewaiting'}

    def render_preparation_datetime(self, value):
        return formatDatetime( value )


class BatchProcessingTable(tables.Table):
    name = tables.LinkColumn('batchDetail',args=[A('pk')])
    modify = tables.LinkColumn('batchModify',args=[A('pk')], text=mark_safe("<img src=\"{:s}\" alt=\"Modify\"/>".format(static('RadiomicsEnabler/images/icon-changelink.svg'))), orderable=False, verbose_name = "Modify" )
    delete = tables.LinkColumn('batchArchive',args=[A('pk')], text=mark_safe("<img src=\"{:s}\" alt=\"Archive\"/>".format(static('RadiomicsEnabler/images/icon-deletelink.svg'))), orderable=False, verbose_name = "Delete" )

    class Meta:
        model = Batch
        sequence = ( 'name', 'extraction_datetime', 'number_of_patients', 'number_of_studies', 'number_of_series', 'number_of_images', 'status', 'size_in_bytes' )
        fields = ( 'name', 'extraction_datetime',  'number_of_patients', 'number_of_studies', 'number_of_series', 'number_of_images', 'status', 'size_in_bytes' )
        order_by = '-extraction_datetime'
        # add class="paleblue" to <table> tag
        attrs = {'class': 'paleblue', 'id':'tableprocessing'}

    def render_size_in_bytes(self, value):
        system = [
            (1024 ** 5, (' PB', ' PBs')),
            (1024 ** 4, (' TB', ' TBs')),
            (1024 ** 3, (' GB', ' GBs')),
            (1024 ** 2, (' MB', ' MBs')),
            (1024 ** 1, (' KB', ' KBs')),
            (1024 ** 0, (' byte', ' bytes')),
        ]
        for factor, suffix in system:
            if value >= factor:
                break
        amount = int(float(value) / factor)
        if isinstance(suffix, tuple):
            singular, multiple = suffix
            if amount == 1:
                suffix = singular
            else:
                suffix = multiple

        if value == 0:
            return "{:d}".format( 0 ) + suffix
        else:
            return "{:.1f}".format( value / factor ) + suffix

    def render_extraction_datetime(self, value):
        return formatDatetime( value )

    def render_status(self, value ):
        return mark_safe('<p class="' + value.lower() + '">' + value + '</p>')


class BatchValidatedTable(tables.Table):
    name = tables.LinkColumn('batchDetail',args=[A('pk')])

    class Meta:
        model = Batch
        sequence = ( 'name', 'extraction_datetime', 'validation_datetime','number_of_patients', 'number_of_studies', 'number_of_series', 'number_of_images' )
        fields = ( 'name', 'extraction_datetime', 'validation_datetime','number_of_patients', 'number_of_studies', 'number_of_series', 'number_of_images' )
        order_by = '-validation_datetime'

        # add class="paleblue" to <table> tag
        attrs = {'class': 'paleblue', 'id':'tablewaiting'}

    def render_extraction_datetime(self, value):
        return formatDatetime( value )

    def render_validation_datetime(self, value):
        return formatDatetime( value )