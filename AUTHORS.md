# Radiomics Enabler® - Unleash the potential of your medical images archives (PACS) for research #

## Authors ##

* Karine Seymour (kseymour@medexprim.com) : Functional design

* Michael Seymour (michael.s.seymour@gmail.com) : Technical design and implementation

Copyright(c)2017-2018 - Medexprim, Toulouse, France - [www.medexprim.com](http://www.medexprim.com)